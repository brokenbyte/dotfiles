#!/bin/bash
#
#shellcheck disable=SC1090,SC2034,SC2005,SC2086,SC2068,SC2046,SC2206

# Reset the sudo timeout do prevent asking for password again or erroring
function sudo_bump() {
  (echo -n $SUDOPASS | sudo -S echo -n "" 2> /dev/null) || (echo "sudo password failed, exiting..." && exit 1)
}

function sudo_install() {
  sudo_bump
  sudo pacman --noconfirm $1 $2
}

function paru_install() {
  sudo_bump
  paru --sudoloop --batchinstall --noconfirm --skipreview -Syu $@
}

############################################################################################################################
############################################################################################################################

CHZM_ROOT="$HOME/.local/share/chezmoi"
INSTALLER="$CHZM_ROOT/install"
AWKSCRIPTS="$INSTALLER/scripts/awk"
SHELLSCRIPTS="$INSTALLER/scripts/shell"
PACKAGES="${INSTALLER}/packages"

REPO_PACKAGES=()
AUR_PACKAGES=()
CARGO_PACKAGES=()

# Enable recursive globbing
shopt -s globstar

# Exit if any commands have nonzero exit status
set -e

# Source all the helper scripts
for f in "$SHELLSCRIPTS"/**/*.sh; do
  source "$f"
done

#shellcheck disable=SC2162
#
# Get a sudo password and make sure it works
read -sp "Enter the password to use for sudo: " SUDOPASS 
echo ""
sudo_bump # validate the password we got

# Figure out which profile we're installing for
PROFILE="$(decode_chassis)"
if [[ $PROFILE = "" ]]; then 
      echo "Unable to determine chassis type, please select one: "
      select chassis in laptop desktop server; do
        PROFILE=$chassis
        break
      done
fi

echo -e "\nChecking for Cargo..."

if [ ! $(command -v cargo) ]; then
  echo -e "\nCargo not found, installing...\n"
  sudo_install -S rustup && rustup default stable
else
  echo -e "\nCargo found"
fi

echo -e "\nChecking for AUR helper..."
if [ ! $(command -v paru) ]; then
  echo -e "\nAUR helper not found, installing...\n"
  command -v git 2>&1  /dev/null || sudo_install -S git
  git clone https://aur.archlinux.org/paru.git
  cd paru
  makepkg -s
  sudo_install -U "*.zst"
fi

# Build the lists of packages to install
get_repo_packages
get_aur_packages
get_cargo_packages

if [[ ${#REPO_PACKAGES[@]} -ne 0 && "${REPO_PACKAGES[0]}" != "" ]]; then
  to_install+=(${REPO_PACKAGES[@]})
  all_packages+=(${REPO_PACKAGES[@]})
fi

if [[ ${#AUR_PACKAGES[@]} -ne 0 && "${AUR_PACKAGES[0]}" != "" ]]; then
  to_install+=(${AUR_PACKAGES[@]})
  all_packages+=(${AUR_PACKAGES[@]})
fi

# Install the packages
NEW_PACKAGES=0
if [[ ${#to_install[@]} -ne 0 && "${to_install[0]}" != "" ]]; then
  echo -e "\nInstalling new packages..."
  NEW_PACKAGES=1

  paru_install "${to_install[@]}"
fi

if [[ ${#CARGO_PACKAGES[@]} -ne 0 && "${CARGO_PACKAGES[0]}" != "" ]]; then
  cargo install ${CARGO_PACKAGES[@]}
  all_packages+=(${CARGO_PACKAGES[@]})
fi


# Run the setup scripts 
if [[ $NEW_PACKAGES ]]; then
  SCRIPTS="$INSTALLER/packages/setup"
  SETUP_DIR=$(mktemp -d)
  SUDO_FILE="$SETUP_DIR"/sudo_setup.sh
  USER_FILE="$SETUP_DIR"/user_setup.sh

  echo "#!/bin/bash" >> "$SUDO_FILE"
  echo "#!/bin/bash" >> "$USER_FILE"
  chmod +x "$SUDO_FILE"
  chmod +x "$USER_FILE"

  export SUDO_FILE
  export USER_FILE

  for f in "$SCRIPTS"/**/*.sh; do
    source "$f"
  done

  echo -e "\nRunning setup scripts..."

  for p in ${all_packages[@]}; do
    declare -F setup_"${p}" && setup_"${p}"
  done

  sudo_bump

  sudo "$SUDO_FILE"
  $USER_FILE
fi

exit 0
