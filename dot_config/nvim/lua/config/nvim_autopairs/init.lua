---@diagnostic disable: unused-function

local npairs = require("nvim-autopairs")
local ts_conds = require("nvim-autopairs.ts-conds")
local rule = require("nvim-autopairs.rule")
local _cond = require("nvim-autopairs.conds")
local _utils = require("nvim-autopairs.utils")

local ts_utils = require("nvim-treesitter.ts_utils")
local _ts_query = vim.treesitter.query

local _print_node = function()
    local target = ts_utils.get_node_at_cursor()
    ---@diagnostic disable-next-line: need-check-nil
    print(target:type())

    return false
end

npairs.setup({
    fast_wrap = {},
    check_ts = true,
    ts_config = {
        rust = { "line_comment", "string" },
    },
    enable_check_bracket_line = true,
})

npairs.add_rule(rule("<", ">", "rust")
    :with_pair(ts_conds.is_ts_node({ "parameters", "struct_item", "type_identifier", "static_item" })) -- Only complete </> when typing generics
    :with_move(function(opts) return opts.rule.end_pair == opts.prev_char end))

local brackets = { { "(", ")" }, { "[", "]" }, { "{", "}" } }
npairs.add_rules({
    rule(" ", " "):with_pair(function(opts)
        local pair = opts.line:sub(opts.col - 1, opts.col)
        return vim.tbl_contains({
            brackets[1][1] .. brackets[1][2],
            brackets[2][1] .. brackets[2][2],
            brackets[3][1] .. brackets[3][2],
        }, pair)
    end),
})
for _, bracket in pairs(brackets) do
    npairs.add_rules({
        rule(bracket[1] .. " ", " " .. bracket[2])
            :with_pair(function() return false end)
            :with_move(function(opts) return opts.prev_char:match(".%" .. bracket[2]) ~= nil end)
            :use_key(bracket[2]),
    })
end

local cond = require("nvim-autopairs.conds")
local utils = require("nvim-autopairs.utils")

local function multiline_close_jump(open, close)
    return rule(close, "")
        :with_pair(function()
            local row, col = utils.get_cursor(0)
            local line = utils.text_get_current_line(0)

            if #line ~= col then --not at EOL
                return false
            end

            local unclosed_count = 0
            for c in line:gmatch("[\\" .. open .. "\\" .. close .. "]") do
                if c == open then unclosed_count = unclosed_count + 1 end
                if unclosed_count > 0 and c == close then unclosed_count = unclosed_count - 1 end
            end
            if unclosed_count > 0 then return false end

            local nextrow = row + 1

            if nextrow < vim.api.nvim_buf_line_count(0) and vim.regex("^\\s*" .. close):match_line(0, nextrow) then
                return true
            end
            return false
        end)
        :with_move(cond.none())
        :with_cr(cond.none())
        :with_del(cond.none())
        :set_end_pair_length(0)
        :replace_endpair(function(opts)
            local row, _col = utils.get_cursor(0)
            local action = vim.regex("^" .. close):match_line(0, row + 1) and "a" or ("0f%sa"):format(opts.char)
            return ("<esc>xj%s"):format(action)
        end)
end

npairs.add_rules({
    multiline_close_jump("(", ")"),
    multiline_close_jump("[", "]"),
    multiline_close_jump("{", "}"),
})
