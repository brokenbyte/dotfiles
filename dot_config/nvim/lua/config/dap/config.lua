local dap = require("dap")

dap.adapters.lldb = {
    type = "executable",
    command = "/usr/bin/lldb-vscode",
    name = "lldb",
}

dap.configurations.cpp = {
    {
        name = "Launch",
        type = "lldb",
        request = "launch",
        program = function()
            return coroutine.create(function(dap_run_co)
                local executables = vim.fn.systemlist([[fd --type=executable --no-ignore]])
                vim.ui.select(executables, {
                    format_item = function(item)
                        local file = item:gsub(vim.fn.getcwd(), "")
                        return "Debug> " .. file
                    end,
                }, function(choice) coroutine.resume(dap_run_co, choice) end)
            end)
        end,

        cwd = "${workspaceFolder}",
        stopOnEntry = false,
        args = {},
    },
}

dap.configurations.c = dap.configurations.cpp
