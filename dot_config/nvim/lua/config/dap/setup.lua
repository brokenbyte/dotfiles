local map = vim.keymap.set

map("n", "<F8>", function() require("dap").toggle_breakpoint() end)
map("n", "<F9>", function() require("dap").continue() end)
map("n", "<F10>", function() require("dap").step_over() end)
map("n", "<F11>", function() require("dap").step_into() end)
map("n", "<F12>", function() require("dap").step_out() end)
map("n", "<leader>dh", function() require("dap.ui.widgets").hover(nil, { border = "rounded" }) end)
map("n", "<leader>dt", function() require("dap").terminate() end)
map("n", "<leader>bb", function() require("dap").toggle_breakpoint() end)

-- Close dap floating windows by pressing `q`
vim.api.nvim_create_autocmd("FileType", {
    group = vim.api.nvim_create_augroup("dap", { clear = true }),
    pattern = "dap-float",
    callback = function() vim.keymap.set("n", "q", "<cmd>q<CR>") end,
})
