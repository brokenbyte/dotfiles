local has_rust_tools, rust_tools = pcall(require, "rust-tools")
local util = require("util")

if has_rust_tools then
    local extension_path = vim.env.HOME .. ".vscode-oss/extensions/vadimcn.vscode-lldb-1.8.1/"
    local codelldb_path = extension_path .. "adapter/codelldb"
    local liblldb_path = extension_path .. "libcodelldb.so"
    rust_tools.setup({
        tools = {
            -- Automatically set inlay hints (type hints)
            autoSetHints = true,

            -- how to execute terminal commands
            -- options right now: termopen / quickfix
            executor = require("rust-tools/executors").quickfix,
            dap = {
                adapter = require("rust-tools.dap").get_codelldb_adapter(codelldb_path, liblldb_path),
            },
            hover_actions = {
                -- the border that is used for the hover window
                -- see vim.api.nvim_open_win()
                border = "rounded",
                max_width = 100,
            },
        },
        -- all the opts to send to nvim-lspconfig
        -- these override the defaults set by rust-tools.nvim
        -- see https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#rust_analyzer
        server = {
            capabilities = util.capabilities,
            on_attach = util.on_attach,
            flags = {
                debounce_text_changes = false,
            },
            settings = {
                ["rust-analyzer"] = {
                    assist = {
                        importGranularity = "crate",
                        importEnforceGranularity = true,
                        importPrefix = "by_crate",
                    },
                    checkOnSave = {
                        command = "clippy",
                    },
                    procMacro = {
                        enable = true,
                    },
                },
            },
        },
    })
end
vim.g.rustaceanvim = {
    tools = {
        -- Automatically set inlay hints (type hints)
        autoSetHints = true,

        -- how to execute terminal commands
        -- options right now: termopen / quickfix
        -- executor = require("rust-tools/executors").quickfix,
        --[[
           [ dap = {
           [     adapter = require("rust-tools.dap").get_codelldb_adapter(codelldb_path, liblldb_path),
           [ },
           ]]
        --[[
           [ hover_actions = {
           [     the border that is used for the hover window
           [     see vim.api.nvim_open_win()
           [     border = "rounded",
           [     max_width = 100,
           [ },
           ]]
        float_win_config = {
            border = "rounded",
        }
    },
    -- all the opts to send to nvim-lspconfig
    -- these override the defaults set by rust-tools.nvim
    -- see https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#rust_analyzer
    server = {
        capabilities = util.capabilities,
        on_attach = util.on_attach,
        flags = {
            debounce_text_changes = false,
        },
        settings = {
            ["rust-analyzer"] = {
                assist = {
                    importGranularity = "crate",
                    importEnforceGranularity = true,
                    importPrefix = "by_crate",
                },
                checkOnSave = {
                    command = "clippy",
                },
                procMacro = {
                    enable = true,
                },
            },
        },
    },
}

-- set `:make` to run `cargo build` in rust files
vim.api.nvim_create_autocmd("FileType", {
    group = vim.api.nvim_create_augroup("Rust", { clear = true }),
    pattern = "rust",
    callback = function() vim.cmd([[ setlocal makeprg=cargo\ build ]]) end,
})

--[[
   [ local bufnr = vim.api.nvim_get_current_buf()
   [ vim.keymap.set("n", "<leader>a", function()
   [     vim.cmd.RustLsp("codeAction") -- supports rust-analyzer's grouping
   [     -- or vim.lsp.buf.codeAction() if you don't want grouping.
   [ end, { silent = true, buffer = bufnr })
   ]]
-- vim.keymap.set("n", "<space>a", vim.cmd.RustLsp('codeAction'))

-- vim.keymap.set("n", "K", "<cmd>lua require'rustaceanvim'.hover_actions.hover_actions()<CR>")
