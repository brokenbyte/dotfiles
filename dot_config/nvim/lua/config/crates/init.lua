require("crates").setup({
    thousands_separator = ",",
    popup = {
        autofocus = true,
        border = "rounded",
    },
})

vim.api.nvim_create_autocmd("BufEnter", {
    group = vim.api.nvim_create_augroup("CargoNvim", { clear = true }),
    pattern = "Cargo.toml",
    callback = function() vim.keymap.set("n", "K", '<cmd>lua require("crates").show_popup()<CR>') end,
})
