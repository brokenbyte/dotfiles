vim.o.fillchars = vim.o.fillchars .. "diff:╱,"

require("diffview").setup({
    enhanced_diff_hl = true,
    hooks = {
        view_opened = function(_bufnr)
            vim.cmd([[Barbecue hide]])
        end,
        diff_buf_read = function(_bufnr)
            vim.cmd([[IndentBlanklineDisable]])
            vim.cmd([[FocusDisable]])
            vim.cmd([[ScrollViewDisable]])
        end,
        view_leave = function(_) vim.cmd([[IndentBlanklineEnable]]) end,
    },
})
