require("mason-lspconfig").setup()

local util = require("util")
local lspconfig = require("lspconfig")

require("mason-lspconfig").setup_handlers({
    function(server_name)
        local opts = {
            on_attach = util.on_attach,
            capabilities = util.capabilities,
            update_in_insert = true,
        }

        local orig_util_open_floating_preview = vim.lsp.util.open_floating_preview
        function vim.lsp.util.open_floating_preview(contents, syntax, _opts, ...)
            opts = _opts or {}
            opts.border = "rounded"
            return orig_util_open_floating_preview(contents, syntax, opts, ...)
        end

        vim.diagnostic.config({
            float = { border = "rounded" },
        })

        lspconfig[server_name].setup(opts)
        require("lspconfig.ui.windows").default_options.border = "rounded"
    end,
    ["html"] = function()
        lspconfig.html.setup({
            on_attach = util.on_attach,
            capabilities = util.capabilities,
            update_in_insert = true,
            init_options = {
                provideFormatter = false, -- I use prettier for formatting with null-ls
            },
        })
    end,
    ["lua_ls"] = function()
        lspconfig.lua_ls.setup({
            on_attach = util.on_attach,
            capabilities = util.capabilities,
            update_in_insert = true,
            settings = {
                Lua = {
                    runtime = {
                        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                        version = "LuaJIT",
                    },
                    diagnostics = {
                        -- Get the language server to recognize the `vim` global
                        globals = { "vim" },
                        -- Don't warn on unused variables starting with _
                        unusedLocalExclude = { "_*" },
                        neededFileStatus = {
                            ["codestyle-check"] = "Any", -- Code style linting
                        },
                    },
                    workspace = {
                        -- Make the server aware of Neovim runtime files
                        library = vim.api.nvim_get_runtime_file("", true),
                        checkThirdParty = false,
                    },
                    format = {
                        enable = false, -- I use stylua for formatting with null-ls
                    },
                    -- Do not send telemetry data containing a randomized but unique identifier
                    telemetry = {
                        enable = false,
                    },
                },
            },
        })
    end,
})
