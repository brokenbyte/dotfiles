vim.g.calvera_italic_comments = true
vim.g.calvera_italic_keywords = true
vim.g.calvera_contrast = false
vim.g.calvera_borders = true

-- The default selection color is almost invisible :(
vim.g.calvera_custom_colors = { selection = "#36366b" }

require("calvera").set()
vim.cmd("colorscheme calvera")

vim.cmd([[ hi ScrollView ctermbg=242 guibg=#424242 ]])

-- diffview.nvim
vim.cmd([[hi DiffText gui=reverse guifg=#7ace5c]])

vim.cmd([[hi FloatBorder guifg=#29295f]])

-- lukas-reineke/indent-blankline.nvim
vim.cmd([[highlight IndentBlanklineContextStart  cterm=underline gui=underline guisp=#5050B9]])

-- nvim-treesitter/nvim-treesitter-context
vim.cmd([[hi TreesitterContextBottom gui=underline guisp=Grey]])
vim.cmd([[hi TreesitterContext guifg=#b0bec5 guibg=#1c1c42]])

-- :LspInfo colors
vim.cmd([[hi LspInfoBorder guifg=#29295f]])

-- nvim-bqf
vim.cmd([[
    hi BqfPreviewBorder guifg=#29295f
    hi link BqfPreviewRange Search
]])

-- quick-scope
vim.cmd([[
highlight QuickScopePrimary guifg='#afff5f' gui=underline ctermfg=155 cterm=underline
highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=81 cterm=underline
]])

-- LSP/vim-illuminate hover colors
vim.cmd([[
    hi LspReferenceRead     cterm=bold guibg=#4b1b89
    hi LspReferenceWrite    cterm=bold guibg=#4b1b89
    hi LspReferenceText     cterm=bold guibg=#4b1b89
    hi IlluminatedWordRead  cterm=bold gui=underline guisp=#4b1b89
    hi IlluminatedWordWrite cterm=bold gui=underline guisp=#4b1b89
    hi IlluminatedWordText  cterm=bold gui=bold,underline guisp=#4b1b89
]])

-- nvim-cmp
require("config.cmp.colorscheme")
