local actions = require("fzf-lua.actions")

require("fzf-lua").setup({
    "telescope",
    commands = {
        actions = {
            ["default"] = actions.ex_run_cr, -- Select and execute the command
            ["alt-enter"] = actions.ex_run, -- Put the command in the cmdline but don't run it
        },
    },
    winopts = {
        hl = { border = "FloatBorder" },
    },
    keymap = {
        builtin = {
            ["<F1>"] = "toggle-help",
            ["<C-f>"] = "preview-page-down",
            ["<C-d>"] = "preview-page-up",
        },
        fzf = {
            ["ctrl-u"] = "unix-line-discard",
            ["ctrl-a"] = "beginning-of-line",
            ["ctrl-e"] = "end-of-line",
            ["ctrl-q"] = "abort",
        },
    },
})
