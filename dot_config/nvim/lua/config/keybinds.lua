-- Disable all arrowkeys
vim.keymap.set({ "v", "n", "i" }, "<Up>", "<Nop>")
vim.keymap.set({ "v", "n", "i" }, "<Down>", "<Nop>")
vim.keymap.set({ "v", "n", "i" }, "<Left>", "<Nop>")
vim.keymap.set({ "v", "n", "i" }, "<Right>", "<Nop>")

-- Allows easier navigation on lines that are broken
vim.keymap.set("n", "j", "gj")
vim.keymap.set("n", "k", "gk")

-- Clear search highlights with ^C
vim.keymap.set("n", "<C-C>", ":nohlsearch<CR>")

-- Start vim-dispatch
vim.keymap.set("n", "<leader>m", ":Make<CR>")

-- Window navigation
vim.keymap.set("n", "<C-J>", "<C-W><C-J>")
vim.keymap.set("n", "<C-K>", "<C-W><C-K>")
vim.keymap.set("n", "<C-L>", "<C-W><C-L>")
vim.keymap.set("n", "<C-H>", "<C-W><C-H>")
vim.keymap.set("n", "<C-Q>", "<C-W><C-Q>")
vim.keymap.set("n", "<C-W>t", ":tabnew<CR>")
vim.keymap.set("n", "<C-W><C-t>", ":tabnew<CR>")

-- Shift-L clears screen since we remapped Ctrl-L
vim.keymap.set("n", "L", ":mode<CR>")

-- Tab/Shift-Tab cycle vim tabs, need to find another way to do this
-- since ^I is used for the jumpstack in vim and it now conflicts with Shift-Tab
vim.keymap.set("n", "<Tab>", "gt")
vim.keymap.set("n", "<S-Tab>", "gT")
