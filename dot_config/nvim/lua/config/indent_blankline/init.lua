local highlight = {
    "RainbowRed",
    "RainbowYellow",
    "RainbowBlue",
    "RainbowOrange",
    "RainbowGreen",
    "RainbowViolet",
    "RainbowCyan",
}

local hooks = require("ibl.hooks")
-- create the highlight groups in the highlight setup hook, so they are reset
-- every time the colorscheme changes
hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
    vim.api.nvim_set_hl(0, "RainbowRed", { fg = "#E06C75" })
    vim.api.nvim_set_hl(0, "RainbowYellow", { fg = "#E5C07B" })
    vim.api.nvim_set_hl(0, "RainbowBlue", { fg = "#61AFEF" })
    vim.api.nvim_set_hl(0, "RainbowOrange", { fg = "#D19A66" })
    vim.api.nvim_set_hl(0, "RainbowGreen", { fg = "#98C379" })
    vim.api.nvim_set_hl(0, "RainbowViolet", { fg = "#C678DD" })
    vim.api.nvim_set_hl(0, "RainbowCyan", { fg = "#56B6C2" })
end)

require("ibl").setup({
    indent = { highlight = highlight },
    --[[
       [ viewport_buffer = 300,
       [ char = "▎",
       [ context_patterns = {
       [     "arrow_function",
       [     "class",
       [     "^func",
       [     "method",
       [     "^if",
       [     "while",
       [     "for",
       [     "with",
       [     "try",
       [     "except",
       [     "match",
       [     "arguments",
       [     "argument_list",
       [     "object",
       [     "dictionary",
       [     "element",
       [     "table",
       [     "tuple",
       [     "do_block",
       [ },
       [ filetype_exclude = {
       [     "man",
       [     "gitmessengerpopup",
       [     "diagnosticpopup",
       [     "lspinfo",
       [     "packer",
       [     "checkhealth",
       [     "TelescopePrompt",
       [     "TelescopeResults",
       [     "tex",
       [     "help",
       [     "json",
       [     "alpha",
       [     "terminal",
       [     "",
       [ },
       [ space_char_blankline = " ",
       [ show_current_context = true,
       [ show_current_context_start = true,
       ]]
})
