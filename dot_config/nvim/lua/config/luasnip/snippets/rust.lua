---@diagnostic disable: undefined-global

local snippets = {
    s({ trig = "struct", dscr = "struct definition" }, {
        d(
            1,
            function(_args)
                return sn(
                    nil,
                    fmt(
                        [[
              struct <name> {
                <>
              }
            ]],
                        {
                            name = i(1),
                            i(2),
                        },
                        {
                            delimiters = "<>",
                        }
                    )
                )
            end
        ),
    }),
}

return snippets
