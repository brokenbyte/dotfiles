---@diagnostic disable: undefined-global

local snippets = {
    s("gdb", {
        t('echo "<pre>";'),
        t({ "", "" }),
        i(1),
        t({ "", "" }),
        t('echo "<pre/>";'),
        t({ "", "" }),
        i(0),
    }),

    s("elm", {
        t('error_log(" ");'),
        t({ "", "" }),
        t("error_log(__METHOD__);"),
        t({ "", "" }),
        t('error_log("    '),
        i(1),
        t(' ");'),
        i(2),
        t({ "", "" }),
        t('error_log(" ");'),
        t({ "", "" }),
        i(0),
    }),

    s("pbt", {
        t("error_log((new \\Exception)->getTraceAsString());"),
    }),
}

return snippets
