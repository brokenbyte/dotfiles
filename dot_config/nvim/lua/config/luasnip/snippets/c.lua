---@diagnostic disable: undefined-global

local snippets = {
    s({ trig = "main", dscr = "main function", name = "main" }, {
        t({
            "int main(",
        }),
        c(1, {
            t("void"),
            t("int argc, char **argv"),
            t("int argc, char **argv, char **envp"),
        }),
        t({
            "){",
            "\t",
        }),
        i(0),
        t({
            "",
            "}",
        }),
    }),

    s("for", {
        t("for("),
        c(1, {
            sn(nil, {
                i(1),
                t(";;"),
            }),
            sn(nil, {
                i(1, "int i = 0"),
                t("; "),
                i(2, "i < 10"),
                t("; "),
                i(3, "i++"),
            }),
        }),
        t({
            "){",
            "\t",
        }),
        i(2),
        t({
            "",
            "}",
        }),
    }),

    s({ trig = "ifn", dscr = "#ifndef ... #endif" }, {
        d(1, function(_args)
            local name = string.upper(vim.fn.expand("%:T"):match("(.+)%..+$"))
            return sn(
                nil,
                fmt(
                    [[
      #ifndef {def}
      #define {def}
      {}
      #endif // {def}
      {}
      ]],
                    {
                        def = i(1, name),
                        i(2),
                        i(3),
                    },
                    {
                        repeat_duplicates = true,
                    }
                )
            )
        end),
    }),

    s({ trig = "ifd", dscr = "#ifdef ... #endif" }, {
        d(1, function(_args)
            local name = string.upper(vim.fn.expand("%:T"):match("(.+)%..+$"))
            return sn(
                nil,
                fmt(
                    [[
        #ifdef {def}
        {}
        #endif // {def}
        {}
        ]],
                    {
                        def = i(1, name),
                        i(2),
                        i(3),
                    },
                    {
                        repeat_duplicates = true,
                    }
                )
            )
        end),
    }),
}

return snippets
