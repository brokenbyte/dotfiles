---@diagnostic disable: undefined-global

local cc_types = {
    "fix",
    "feat",
    "build",
    "chore",
    "ci",
    "docs",
    "style",
    "refactor",
    "perf",
    "test",
}

local snippets = {}
for _, type in pairs(cc_types) do
    table.insert(
        snippets,
        s({ trig = type, dscr = type .. " conventional commit message", name = type }, {
            c(1, {
                sn(nil, {
                    t(type),
                    i(1),
                }),
                sn(nil, {
                    t(type .. "("),
                    i(1),
                    t(")"),
                }),
            }),
            t(": "),
            i(2, "title"),
        }, {
            -- Only show these completions if they're the first thing in the file
            show_condition = function(_line_to_cursor)
                local row, col = unpack(vim.api.nvim_win_get_cursor(0))
                print(row, col)
                return (row == 1) and (col <= 1)
            end,
        })
    )
end

return snippets
