require("lualine").setup({
    options = {
        icons_enabled = true,
        theme = "calvera-nvim",
        component_separators = { left = "", right = "" },
        section_separators = { left = "", right = "" },
        disabled_filetypes = {},
        always_divide_middle = false,
        globalstatus = true,
    },
    sections = {
        lualine_a = { "mode" },
        lualine_b = {
            "branch",
            {
                "diff",
                symbols = { added = "", modified = "𝝙", removed = "󰍴" },
            },
            {
                "diagnostics",
                sources = { "nvim_diagnostic" },
            },
        },
        lualine_c = {
            {
                "filename",
                path = 0,
            },
        },
        lualine_x = { "encoding", "fileformat", "filetype" },
        lualine_y = { "progress" },
        lualine_z = { "location" },
    },
    --[[
       [ Not used with globalstatus = true (above),
       [ which sets vim.opt.laststatus = 3 for a global statusline
       [
       [ inactive_sections = {
       [     lualine_a = {},
       [     lualine_b = {},
       [     lualine_c = { "filename" },
       [     lualine_x = { "location" },
       [     lualine_y = {},
       [     lualine_z = {},
       [ },
       ]]
    tabline = {
        lualine_a = {
            {
                "tabs",
                max_length = function() return math.floor(vim.o.columns * 0.80) end,
                mode = 2,
                fmt = function(name, context)
                    -- Show in tab ● if buffer is modified
                    local buflist = vim.fn.tabpagebuflist(context.tabnr)
                    local winnr = vim.fn.tabpagewinnr(context.tabnr)
                    local bufnr = buflist[winnr]
                    local mod = vim.fn.getbufvar(bufnr, "&mod")

                    -- Reserve space for the icon so the tab doesn't jump around
                    return name .. (mod == 1 and " ●" or "  ")
                end,
            },
        },
        lualine_b = {},
        lualine_c = {},
        lualine_x = {},
        lualine_y = {},
        lualine_z = {},
    },
})
