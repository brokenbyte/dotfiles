-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    local lazyrepo = "https://github.com/folke/lazy.nvim.git"
    local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
    if vim.v.shell_error ~= 0 then
        vim.api.nvim_echo({
            { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
            { out, "WarningMsg" },
            { "\nPress any key to exit..." },
        }, true, {})
        vim.fn.getchar()
        os.exit(1)
    end
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    spec = {
        ----------------------------------------------------------
        --                   Utility Plugins                    --
        ----------------------------------------------------------
        { "lewis6991/impatient.nvim" }, -- Decrease nvim load times

        { "dstein64/vim-startuptime" }, -- Measure startup time

        { "nvim-lua/plenary.nvim" }, -- Neovim Lua functions

        --[[
           [ use({
           [     "nvim-neorg/neorg",
           [     config = function()
           [         require("neorg").setup({
           [             load = {
           [                 ["core.defaults"] = {}, -- Loads default behaviour
           [                 ["core.norg.concealer"] = {}, -- Adds pretty icons to your documents
           [                 ["core.norg.dirman"] = { -- Manages Neorg workspaces
           [                     config = {
           [                         workspaces = {
           [                             notes = "~/notes",
           [                         },
           [                     },
           [                 },
           [                 ["core.norg.completion"] = {
           [                     config = {
           [                         engine = "nvim-cmp",
           [                     },
           [                 },
           [                 ["core.integrations.treesitter"] = {},
           [             },
           [         })
           [     end,
           [     run = ":Neorg sync-parsers",
           [     requires = "nvim-lua/plenary.nvim",
           [     ft = "norg",
           [ })
           ]]

        {
            "nvim-treesitter/nvim-treesitter", -- Tree-sitter integration
            build = ":TSUpdate",
            config = function() require("config.treesitter") end,
        },

        { "tpope/vim-repeat", event = "BufRead" }, -- Allows the 'dot' command to repeat plugin actions

        { "tpope/vim-surround", event = "BufRead" }, -- Surround objects with quotes/brackets/braces

        { "tpope/vim-eunuch", event = "BufRead" }, -- Vim wrapper for common shell commands

        { "zenbro/mirror.vim", event = "BufRead" }, -- Upload/download files via SSH

        {
            "ibhagwan/fzf-lua", -- FZF integration
            init = function()
                vim.keymap.set("n", "<C-p>", function() require("fzf-lua").commands() end)
                vim.keymap.set("n", "<C-f>", function() require("fzf-lua").files() end)
                vim.keymap.set("n", "<C-b>", function() require("fzf-lua").buffers() end)
                vim.keymap.set("n", "<C-s>", function() require("fzf-lua").grep_project() end)
                vim.keymap.set("n", "<space>p", function() require("fzf-lua").builtin() end)
            end,
            config = function() require("config.fzf_lua") end,
        },

        { "mbbill/undotree", cmd = "UndotreeToggle" }, -- Interactive undo-tree navigation

        {
            "nvim-telescope/telescope.nvim", -- Universal fuzzy finder
            config = function()
                require("telescope").setup({
                    extensions = {
                        ["ui-select"] = {
                            require("telescope.themes").get_dropdown(),
                        },
                    },
                })
                require("telescope").load_extension("ui-select")
            end,
            event = "BufRead",
        },

        {
            "akinsho/toggleterm.nvim",
            config = function()
                require("toggleterm").setup({
                    open_mapping = "<F2>",
                    direction = "float",
                    float_opts = {
                        winblend = 20,
                    },
                })
            end,
        },

        {
            "kelly-lin/ranger.nvim",
            config = function()
                require("ranger-nvim").setup({ replace_netrw = true })
                vim.api.nvim_set_keymap("n", "<leader>r", "", {
                    noremap = true,
                    callback = function() require("ranger-nvim").open(true) end,
                })
            end,
        },

        {
            "neovim/nvim-lspconfig", -- Nvim LSP configuration tool
            config = function() require("config.lsp_config") end,
        },

        -- use({ "andweeb/presence.nvim", event = "BufRead" }) -- Discord rich presence

        { "junegunn/vim-slash", event = "BufRead" }, -- Enhanced '/' searching

        { "tpope/vim-unimpaired", event = "BufRead" }, -- Additional Vim navigation mappings

        {
            "booperlv/nvim-gomove", -- Move blocks around in visual mode
            config = function() require("gomove").setup({ move_past_end_col = true }) end,
            event = "BufRead",
        },

        {
            "nacro90/numb.nvim", -- Preview lines with :N without actually jumping there
            config = function() require("numb").setup() end,
            event = "CmdlineEnter",
        },

        { "wellle/targets.vim", event = "BufRead" }, -- Enhanced text objects

        { "chaoren/vim-wordmotion", event = "BufRead" }, -- Enhanced word objects

        {
            "junegunn/vim-after-object", -- Adds an 'after' object, e.g. ca= and ca:
            config = function() vim.cmd([[autocmd VimEnter * call after_object#enable('=', ':', '-', '#')]]) end,
            event = "BufRead",
        },

        -----------------------------------------------------
        --                   Development                   --
        -----------------------------------------------------
        --------------------------
        --     Autocomplete     --
        --------------------------

        {
            "hrsh7th/nvim-cmp", -- Completion engine
            config = function() require("config.cmp") end,
            dependencies = {
                { "hrsh7th/cmp-cmdline", event = "CmdlineEnter" }, -- Command line completion source
                { "hrsh7th/cmp-nvim-lsp", event = "LspAttach" }, -- LSP completion source
                { "hrsh7th/cmp-buffer", after = "nvim-cmp" }, -- Buffer completion source
                { "hrsh7th/cmp-path", after = "nvim-cmp" }, -- File path completion source
                { "hrsh7th/cmp-nvim-lua", ft = "lua" }, -- Nvim Lua API completion
                { "hrsh7th/cmp-calc", event = "InsertEnter" }, -- In-buffer calculations ( 2+2 = 4 )
                { "saadparwaiz1/cmp_luasnip", event = "InsertEnter" }, -- LuaSnip completion source
                { "kristijanhusak/vim-dadbod-completion", ft = "sql" }, -- Database completion
            },
            event = { "InsertEnter", "CmdlineEnter" },
        },

        {
            "L3MON4D3/LuaSnip", -- Snippet plugin
            config = function() require("config.luasnip") end,
            build = "make install_jsregexp",
            event = "InsertEnter",
        },

        {
            "Saecki/crates.nvim", -- Completion for Cargo.toml files
            config = function() require("config.crates") end,
            ft = "toml",
        },

        {
            "windwp/nvim-autopairs", -- Auto brace/bracket/paren closing
            config = function() require("config.nvim_autopairs") end,
            after = "nvim-treesitter",
        },

        {
            "windwp/nvim-ts-autotag",
            config = function() require("nvim-ts-autotag").setup() end,
            dependencies = "nvim-treesitter",
        }, -- Auto brace/bracket/paren closing

        -------------------------------
        --     Language Specific     --
        -------------------------------

        {
            "mrcjkb/rustaceanvim",
            version = "^5", -- Recommended
            lazy = false, -- This plugin is already lazy
            config = function() require("config.rust_tools") end,
        },

        {
            "ray-x/go.nvim", -- Golang settings/tooling
            config = function() require("go").setup() end,
            ft = "go",
        },

        {
            "lervag/vimtex", -- LaTeX completion/tooling
            init = function()
                vim.g.vimtex_compiler_progname = "nvr"
                vim.g.tex_flavor = "latex"
                vim.g.vimtex_quickfix_mode = false
                vim.g.vimtex_syntax_enabled = false
                vim.g.vimtex_syntax_conceal_disable = true
                vim.g.vimtex_fold_enabled = true
                vim.g.vimtex_view_general_viewer = "okular"
                vim.g.vimtex_view_general_options = "--unique file:@pdf\\#src:@line@tex"
                -- vim.o.fillchars = vim.o.fillchars .. "fold: ,"
            end,
            ft = "latex",
        },

        -- use({ "tpope/vim-dadbod", ft = "sql" }) -- Database plugin

        --[[
           [ use({
           [     "iamcco/markdown-preview.nvim", -- Live markdown rendering
           [     setup = function() vim.g.mkdp_filetypes = { "markdown" } end,
           [     run = "cd app && npm install",
           [     ft = { "markdown" },
           [ })
           ]]

        -----------------------
        --     Debugging     --
        -----------------------

        {
            "mfussenegger/nvim-dap", -- Debugger Adapter Protocol plugin (LSP for debuggers)
            init = function() require("config.dap.setup") end,
            config = function() require("config.dap.config") end,
        },

        {
            "rcarriga/nvim-dap-ui", -- Nice UI for nvim-dap
            config = function()
                local dap, dapui = require("dap"), require("dapui")

                dapui.setup()

                dap.listeners.after.event_initialized["dapui_config"] = function() dapui.open() end
                dap.listeners.before.event_terminated["dapui_config"] = function() dapui.close() end
                dap.listeners.before.event_exited["dapui_config"] = function() dapui.close() end
            end,
            dependencies = {
                { "nvim-dap" },
                { "nvim-neotest/nvim-nio" },
            },
        },

        ------------------------
        --     Formatting     --
        ------------------------

        {
            "mhartington/formatter.nvim", -- File formatter
            config = function()
                local util = require("formatter.util")

                require("formatter").setup({
                    filetype = {
                        lua = {
                            function()
                                return {
                                    exe = "stylua",
                                    args = {
                                        "--search-parent-directories",
                                        "--stdin-filepath",
                                        util.escape_path(util.get_current_buffer_file_path()),
                                        "--",
                                        "-",
                                    },
                                    stdin = true,
                                }
                            end,
                        },
                        rust = {
                            require("formatter.filetypes.rust").rustfmt,
                        },
                        php = {
                            require("formatter.filetypes.php").php_cs_fixer,
                        },
                        go = {
                            require("formatter.filetypes.go").gofmt,
                        },
                        ["*"] = {
                            require("formatter.filetypes.any").remove_trailing_whitespace,
                        },
                    },
                })
            end,
            cmd = "Format",
        },

        {
            "junegunn/vim-easy-align", -- Quickly align around a character
            setup = function()
                vim.keymap.set("n", "<leader>ga", "<Plug>(EasyAlign)")
                vim.keymap.set("x", "<leader>ga", "<Plug>(EasyAlign)")
            end,
            event = "BufRead",
        },

        ------------------------
        --     Navigation     --
        ------------------------

        {
            "stevearc/aerial.nvim", -- Symbol tree
            init = function() vim.keymap.set("n", "<Space>s", ":AerialToggle<CR>") end,
            config = function()
                require("aerial").setup({
                    default_direction = "prefer_right",
                    filter_kind = false,
                    highlight_on_hover = true,
                    manage_folds = true,
                })
            end,
            cmd = "AerialToggle",
        },

        { "RRethy/nvim-treesitter-textsubjects", dependencies = { "nvim-treesitter" } },

        { "nvim-treesitter/nvim-treesitter-textobjects", dependencies = { "nvim-treesitter" } },

        { "RRethy/vim-illuminate" }, -- Highlight word under cursor via LSP/TS/Regex

        {
            "rmagatti/goto-preview", -- go to definition in a pop up window
            config = function()
                local gtp = require("goto-preview")

                gtp.setup({
                    height = 30,
                })

                vim.keymap.set("n", "gpd", gtp.goto_preview_definition)
                vim.keymap.set("n", "gpt", gtp.goto_preview_type_definition)
                vim.keymap.set("n", "gpi", gtp.goto_preview_implementation)
                vim.keymap.set("n", "gpr", gtp.goto_preview_references)
                vim.keymap.set("n", "gP", gtp.close_all_win)
            end,
            event = "BufRead",
        }, -- View definitions in a pop up window

        {
            "folke/trouble.nvim", -- Diagnostics list/aggregator
            config = function()
                require("trouble").setup({
                    action_keys = {
                        open_split = { "<c-s>" },
                    },
                })
            end,
            cmd = { "Trouble", "TroubleToggle" },
        },

        {
            "unblevable/quick-scope", -- Faster left/right movement
            init = function()
                vim.g.qs_highlight_on_keys = { "f", "F", "t", "T" }
                vim.g.qs_lazy_highlight = true
            end,
            keys = {
                { "n", "f" },
                { "n", "F" },
                { "n", "t" },
                { "n", "T" },
            },
        },

        ------------------
        --     Misc     --
        ------------------

        {
            "preservim/nerdcommenter", -- Comment helper
            init = function() vim.g.NERDSpaceDelims = 1 end,
            event = "BufRead",
        },

        {
            "kevinhwang91/nvim-ufo",
            dependencies = { "kevinhwang91/promise-async" },
            module = "ufo",
        }, -- Smarter folding

        -- use({ "manzeloth/live-server", cmd = "LiveServer" }) -- Live webpage reload

        {
            "nvimtools/none-ls.nvim", -- General purpose LSP things
            config = function()
                local null_ls = require("null-ls")

                null_ls.setup({
                    debug = false,
                    sources = {
                        null_ls.builtins.formatting.prettier,
                        null_ls.builtins.formatting.stylua,
                    },
                })
            end,
            event = "BufRead",
        },

        -- use({ "tpope/vim-dispatch", cmd = "Make" }) -- Build helper

        {
            "williamboman/mason.nvim",
            config = function()
                require("mason").setup({
                    ui = {
                        border = "rounded",
                    },
                })
            end,
            cmd = "Mason",
            dependencies = {
                "williamboman/mason-lspconfig.nvim",
                config = function() require("config.mason_lsp") end,
                event = "BufRead",
            },
        },

        -----------------------------------------------------
        --                   Git Plugins                   --
        -----------------------------------------------------

        {
            "tpope/vim-fugitive",
            event = "BufRead",
        }, -- git wrapper for vim

        {
            "ruifm/gitlinker.nvim", -- Copy git permalinks to clipboard
            config = function()
                require("gitlinker").setup({})

                vim.api.nvim_set_keymap(
                    "n",
                    "<leader>gb",
                    '<cmd>lua require"gitlinker".get_buf_range_url("n", {action_callback = require"gitlinker.actions".open_in_browser})<cr>',
                    { silent = true }
                )
                vim.api.nvim_set_keymap(
                    "v",
                    "<leader>gb",
                    '<cmd>lua require"gitlinker".get_buf_range_url("v", {action_callback = require"gitlinker.actions".open_in_browser})<cr>',
                    { silent = true }
                )
            end,
            event = "BufRead",
        },

        {
            "lewis6991/gitsigns.nvim", -- Show file additions/removes/changes in the sign column
            config = function() require("config.gitsigns") end,
            event = "BufRead",
        },

        {
            "TimUntersberger/neogit", -- git UI
            config = function()
                require("neogit").setup({
                    integrations = {
                        diffview = true,
                    },
                })
            end,
            cmd = "Neogit",
        },

        {
            "sindrets/diffview.nvim", -- Enhanced diff management
            config = function() require("config.diffview") end,
            cmd = "DiffviewOpen",
        },

        {
            "rhysd/git-messenger.vim", -- View git commit messages for line under the cursor
            config = function()
                vim.g.git_messenger_floating_win_opts = { border = "rounded" }
                vim.keymap.set("n", "<leader>gm", ":GitMessenger<CR>")
            end,
            cmd = "GitMessenger",
        },

        ----------------------------------------------------
        --                   UI Plugins                   --
        ----------------------------------------------------

        {
            "nvim-treesitter/nvim-treesitter-context", -- Show current context via TreeSitter
            config = function()
                require("treesitter-context").setup({
                    pattern = {
                        tex = {
                            "minted_environment",
                        },
                    },
                })
            end,
            dependencies = "nvim-treesitter",
        },

        {
            "ray-x/lsp_signature.nvim", -- Function signature previews
            config = function()
                local cfg = {
                    doc_lines = 0,
                }
                require("lsp_signature").setup(cfg)
            end,
            event = "BufRead",
        },

        { "nvim-telescope/telescope-ui-select.nvim" }, -- Cleaner vim.ui.select UI

        { "onsails/lspkind-nvim" }, -- vscode-like pictograms to neovim built-in lsp

        {
            "NvChad/nvim-colorizer.lua", -- Highlight colors with their color: #ababab
            config = function()
                require("colorizer").setup({ filetypes = { "*", "!packer", "!mason", "!checkhealth" } })
            end,
            event = "BufRead",
        },

        {
            "goolord/alpha-nvim", -- nvim start screen
            config = function() require("config.alpha") end,
            event = "VimEnter",
        },

        {
            "beauwilliams/focus.nvim", -- Auto resize windows
            config = function()
                require("focus").setup({
                    excluded_filetypes = { "toggleterm", "fzf" },
                    signcolumn = false,
                })
            end,
            event = "BufRead",
        },

        {
            "yashguptaz/calvera-dark.nvim", -- Color scheme
            config = function() require("config.colorscheme") end,
        },

        {
            "nvim-lualine/lualine.nvim", -- Fancy status line/tab line
            config = function() require("config.lualine") end,
            dependencies = "calvera-dark.nvim",
        },

        {
            "utilyre/barbecue.nvim", -- Context breadcrumbs in winbar
            dependencies = {
                { "SmiteshP/nvim-navic" },
            },
            config = function()
                require("barbecue").setup({
                    attach_navic = false, -- prevent barbecue from automatically attaching nvim-navic
                })
            end,
            event = "LspAttach",
        },

        { "fladson/vim-kitty" }, -- Kitty terminal syntax highlighting

        {
            "kevinhwang91/nvim-bqf", -- Improved quickfix window
            config = function()
                require("bqf").setup({
                    auto_enable = true,
                    func_map = {
                        pscrollup = "<C-d>",
                        pscrolldown = "<C-f>",
                        split = "<C-s>",
                    },
                })
            end,
            event = "BufRead",
        },

        {
            "folke/todo-comments.nvim", -- Todo comment highlighting
            config = function() require("todo-comments").setup() end,
            event = "BufRead",
        },

        {
            "dstein64/nvim-scrollview", -- Scroll bar
            config = function()
                vim.g.scrollview_mode = "flexible"
                vim.g.scrollview_windblend = 0
            end,
            event = "BufRead",
        },

        {
            "kyazdani42/nvim-web-devicons",
            event = "BufRead",
        }, -- NerdFonts icons

        {
            "hiphish/rainbow-delimiters.nvim",
            init = function()
                -- This module contains a number of default definitions
                local rainbow_delimiters = require("rainbow-delimiters")

                ---@type rainbow_delimiters.config
                vim.g.rainbow_delimiters = {
                    strategy = {
                        [""] = rainbow_delimiters.strategy["global"],
                        vim = rainbow_delimiters.strategy["local"],
                    },
                    query = {
                        [""] = "rainbow-delimiters",
                        lua = "rainbow-blocks",
                    },
                    priority = {
                        [""] = 110,
                        lua = 210,
                    },
                    highlight = {
                        "RainbowDelimiterRed",
                        "RainbowDelimiterYellow",
                        "RainbowDelimiterBlue",
                        "RainbowDelimiterOrange",
                        "RainbowDelimiterGreen",
                        "RainbowDelimiterViolet",
                        "RainbowDelimiterCyan",
                    },
                }
            end,
            dependencies = "nvim-treesitter",
        }, -- Highlight bracket pairs in colors

        { "kristijanhusak/vim-dadbod-ui", ft = "sql" }, -- Database UI

        {
            "psliwka/vim-smoothie", -- Smooth scrolling
            keys = { "<C-D>", "<C-U>" },
        },

        {
            "lukas-reineke/indent-blankline.nvim", -- Indent lines
            config = function() require("config.indent_blankline") end,
            before = "calvera-dark.nvim",
        },

        {
            "rcarriga/nvim-notify", -- Notification UI
            config = function() vim.notify = require("notify") end,
            event = "BufRead",
        },

        {
            "j-hui/fidget.nvim", -- Show LSP loading status
            config = function() require("fidget").setup({}) end,
        },
    },
})
