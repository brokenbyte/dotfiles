[toc]

> This file documents useful keybinds and commands, both default and customized,
> from the plugins I use the most.

Legend:

- I: Insert mode
- N: Normal mode
- C: Command mode (`:`)
- V: Visual mode

# Vim Mappings

> These are mappings for normal vim commmands, not plugins

| Mode | Mapping   | Effect           |
| ---- | --------- | ---------------- |
| N    | `j`       | `gj`             |
| N    | `k`       | `gk`             |
| N    | `<C-C>`   | `nohlsearch<CR>` |
| N    | `<C-J>`   | `<C-W><C-J>`     |
| N    | `<C-K>`   | `<C-W><C-K>`     |
| N    | `<C-L>`   | `<C-W><C-L>`     |
| N    | `<C-H>`   | `<C-W><C-H>`     |
| N    | `<C-H>`   | `<C-W><C-H>`     |
| N    | `L`       | `:mode<CR>`      |
| N    | `<Tab>`   | `gt`             |
| N    | `<S-Tab>` | `gT`             |

# Utility

- [vim-surround](https://github.com/tpope/vim-surround)

  | Mode | Mapping  | Effect                               |
  | ---- | -------- | ------------------------------------ |
  | N    | `ds"`    | Delete surrounding `"`               |
  | N    | `cs]}`   | Change surrounding `]` to `}`        |
  | N    | `ysiw)`  | Select `iw`, surround in `)`         |
  | N    | `yss)`   | Surround current line in `)`         |
  | N    | `cst<p>` | Change surrounding HTML tag to `<p>` |

- [vim-eunuch](https://github.com/tpope/vim-eunuch)

  | Mode | Mapping   | Effect                                |
  | ---- | --------- | ------------------------------------- |
  | C    | `:Remove` | Delete the file and reload the buffer |
  | C    | `:Delete` | Delete the file and the buffer        |
  | C    | `:Rename` | Rename the file and the buffer        |
  | C    | `:Wall`   | Save all open windows                 |

  If you type a line at the beginning of a file that starts with `#!` and press
  `<CR>`, The current file type will be re-detected.

  Additionally, if the shebang line lacks a path (e.g., `#!bash`), it will be
  normalized by adding `/usr/bin/env` (e.g., `#!/usr/bin/env bash`). If it
  lacks a command entirely (just `#!`), Eunuch will invert the process and pick
  a command appropriate for the current file type. For example, if the file
  type is "python", the shebang will become `#!/usr/bin/env python3` .

  Finally, adding a shebang line to a new or existing file will cause `chmod +x`
  to be invoked on the file on the next write.

- [mirror.vim](https://github.com/zenbro/mirror.vim)

  | Mode | Mapping              | Effect                                |
  | ---- | -------------------- | ------------------------------------- |
  | C    | `:MirrorConfig`      | Edit the environment config file      |
  | C    | `:MirronEnvironment` | Switch mirror environments            |
  | C    | `:MirrorPush`        | Upload the current file to remote     |
  | C    | `:MirrorPull`        | Download the current file from remote |
  | C    | `:MirrorDiff`        | Diff the current file with the remote |

- [fzf-lua](https://github.com/ibhagwan/fzf-lua/)

  | Mode | Mapping    | Effect                                      |
  | ---- | ---------- | ------------------------------------------- |
  | N    | `<C-P>`    | Search `:` commands                         |
  | N    | `<C-F>`    | Search files recursively below nvim's `cwd` |
  | N    | `<C-B>`    | Search open buffers                         |
  | N    | `<C-S>`    | Search project files with `rg`              |
  | N    | `<Space>p` | Search `fzf-lua` built ins                  |
  | N    | `<F2>`     | Toggle fullscreen                           |
  | N    | `<F3>`     | Next terminal window                        |
  | N    | `<F4>`     | Toggle line wrapping in preview window      |
  | N    | `<F5>`     | Cycle window layout                         |

  In the search window:

  | Mapping            | Effect                           |
  | ------------------ | -------------------------------- |
  | `<F1>`             | Toggle help                      |
  | `<C-F>`            | Scroll preview down              |
  | `<C-D>`            | Scroll preview up                |
  | `<C-U>`            | Clear search text                |
  | `<C-A>`            | Jump to beginning of search text |
  | `<C-E>`            | Jump to end of search text       |
  | `<C-Q>` \| `<Esc>` | Close search window              |

  When searching commands (`<C-p>`), `<CR>` will execute the selected command immediately and `<M-CR>`
  will place the command in the cmdline for you to add arguments.

- [undotree](https://github.com/mbbill/undotree/)

  | Mode | Mapping           | Effect                     |
  | ---- | ----------------- | -------------------------- |
  | C    | `:UndotreeToggle` | Toggle undotree visibility |

  In the tree window:

  | Mapping | Effect                     |
  | ------- | -------------------------- |
  | `<CR>`  | View selected history node |
  | `J`     | View next history node     |
  | `K`     | View previous history node |

- [vim-floaterm (might switch to toggleterm?)](https://github.com/voldikss/vim-floaterm)

  | Mode | Mapping | Effect                 |
  | ---- | ------- | ---------------------- |
  | N    | `<F2>`  | Toggle terminal window |
  | N    | `<F3>`  | Next terminal window   |
  | N    | `<F4>`  | Prev terminal window   |
  | N    | `<F5>`  | New terminal window    |

- [nvim-treesitter/playground](https://github.com/nvim-treesitter/playground)

  | Mode | Mapping               | Effect                 |
  | ---- | --------------------- | ---------------------- |
  | C    | `:TSPlaygroundToggle` | Toggle node visibility |

  In the tree window:

  | Mapping | Effect                            |
  | ------- | --------------------------------- |
  | `R`     | Refresh the window                |
  | `o`     | toggle query editor               |
  | `<CR>`  | Go to the node in the code buffer |

- [vim-unimpaired](https://github.com/tpope/vim-unimpaired)

  | Mode | Mapping    | Effect                                |
  | ---- | ---------- | ------------------------------------- |
  | N    | `[a`       | `:previous`                           |
  | N    | `]a`       | `:next`                               |
  | N    | `[A`       | `:first`                              |
  | N    | `]A`       | `:last`                               |
  | N    | `[q`       | `:cprevious`                          |
  | N    | `]q`       | `:cnext`                              |
  | N    | `[Q`       | `:cfirst`                             |
  | N    | `]Q`       | `:clast`                              |
  | N    | `[Q`       | `:cfirst`                             |
  | N    | `]n`       | Next git conflict marker              |
  | N    | `[n`       | Prev git conflict marker              |
  | N    | `[<Space>` | Add [count] blank lines above cursor] |
  | N    | `]<Space>` | Add [count] blank lines below cursor] |
  | N    | `[x`       | XML encode                            |
  | N    | `]x`       | XML decode                            |
  | N    | `[u`       | URL encode                            |
  | N    | `]u`       | URL decode                            |
  | N    | `[y`       | C string encode                       |
  | N    | `]y`       | C string decode                       |

- [nvim-gomove](https://github.com/booperlv/nvim-gomove)

  These mapping work in all modes (normal, visual, line, block) and behave as you'd expect

  | Mapping | Effect     |
  | ------- | ---------- |
  | `<A-H>` | Move left  |
  | `<A-J>` | Move down  |
  | `<A-K>` | Move up    |
  | `<A-L>` | Move right |

  `<A-S-{hjkl}>` will duplicate instead of move

- [vim-after-object](https://github.com/junegunn/vim-after-object)

  I have this plugin configured to add 4 additional objects, one for each of `=`, `:`, `-`, and `#`.
  With this you can type `ca=` in normal mode to `c`hange everything `a`fter the next `=` on the line.

# Development

## Autocomplete

- [nvim-cmp](https://github.com/hrsh7th/nvim-cmp)

  | Mode  | Mapping     | Effect                            |
  | ----- | ----------- | --------------------------------- |
  | I,C,V | `<C-N>`     | Next menu item                    |
  | I,C,V | `<C-P>`     | Prev menu item                    |
  | I,C,V | `<C-Space>` | Trigger completion menu           |
  | I,C,V | `<C-E>`     | Close completion menu             |
  | I,C,V | `<Tab>`     | Select the first completion entry |
  | I     | `<C-D>`     | Scroll docs down                  |
  | I     | `<C-F>`     | Scroll docs up                    |

- [LuaSnip](https://github.com/L3MON4D3/LuaSnip)

  | Mode | Mapping   | Effect                            |
  | ---- | --------- | --------------------------------- |
  | I,V  | `<C-K>`   | Next jump point                   |
  | I,V  | `<C-J>`   | Prev jump point                   |
  | I    | `<Tab>`   | Next choice (when in choice node) |
  | I    | `<S-Tab>` | Prev choice (when in choice node) |

- [crates.nvim](https://github.com/Saecki/crates.nvim)

  | Mode | Mapping | Effect                      |
  | ---- | ------- | --------------------------- |
  | N    | `K`     | Show popup                  |
  | N    | `<CR>`  | Select item in popup window |

## Language Specific

- [vimtex](https://github.com/lervag/vimtex)

  | Mode | Mapping      | Effect                                             |
  | ---- | ------------ | -------------------------------------------------- |
  | N    | `<leader>lt` | Open TOC                                           |
  | N    | `<leader>lv` | View PDF in system viewer                          |
  | N    | `<leader>ll` | Compile and open the project                       |
  | N    | `<leader>le` | View build errors                                  |
  | N    | `<leader>lo` | View compile output                                |
  | N    | `<leader>lc` | Clean temp build files                             |
  | N    | `<leader>lC` | Clean all build files (pdf included)               |
  | N    | `dse`        | Delete surrounding environment                     |
  | N    | `dsc`        | Delete surrounding command                         |
  | N    | `dsd`        | Delete surrounding delimiter                       |
  | N    | `ds$`        | Delete surrounding math                            |
  | N    | `cse`        | Change surrounding environment                     |
  | N    | `csc`        | Change surrounding command                         |
  | N    | `csd`        | Change surrounding delimiter                       |
  | N    | `cs$`        | Change surrounding math                            |
  | N    | `tsf`        | Toggle fraction                                    |
  | N    | `tsc`        | Toggle command star                                |
  | N    | `tse`        | Toggle environment star                            |
  | N    | `ts$`        | Toggle math environment                            |
  | N    | `K`          | Documentation for package/command/env under cursor |
  | I    | `]]`         | Close currently open environment                   |

- [markdown-preview.nvim](https://github.com/iamcco/markdown-preview.nvim)

  | Mode | Mapping            | Effect                           |
  | ---- | ------------------ | -------------------------------- |
  | C    | `:MarkdownPreview` | Preview markdown file in browser |

## Debugging

- [nvim-dap](https://github.com/mfussenegger/nvim-dap)

  | Mode | Mapping      | Effect                            |
  | ---- | ------------ | --------------------------------- |
  | N    | `<F8>`       | Toggle breakpoint on current line |
  | N    | `<F9>`       | DAP continue                      |
  | N    | `<F10>`      | DAP step over                     |
  | N    | `<F11>`      | DAP step into                     |
  | N    | `<F12>`      | DAP step out                      |
  | N    | `<leader>dh` | DAP hover                         |
  | N    | `<leader>dt` | DAP terminate                     |
  | N    | `<leader>bb` | Toggle breakpoint on current line |

## Formatting

- [vim-easy-align](https://github.com/junegunn/vim-easy-align)

  | Mode | Mapping      | Effect           |
  | ---- | ------------ | ---------------- |
  | V    | `<leader>ga` | Begin easy align |

  You can also use `<leader>ga` with a text object in normal mode, e.g. `<leader>gaip`
  to align in a paragraph.

## Navigation

- [aerial.nvim](https://github.com/stevearc/aerial.nvim)

  | Mode | Mapping       | Effect                                  |
  | ---- | ------------- | --------------------------------------- |
  | C    | `:AerialOpen` | Open symbol tree for the current window |

  In the Aerial window:

  | Mapping | Effect                                                       |
  | ------- | ------------------------------------------------------------ |
  | `<CR>`  | Jump to the symbol in the file and leave the Aerial window   |
  | `<C-K>` | Jump to the next symbol in the file                          |
  | `<C-J>` | Jump to the previous symbol in the file                      |
  | `<C-S>` | Jump to the previous symbol in a split                       |
  | `<C-V>` | Jump to the previous symbol in a vertical split              |
  | `p`     | Jump to the symbol in the file but stay in the Aerial window |
  | `q`     | Close the Aerial window                                      |

- [nvim-treesitter-textsubjects](https://github.com/RRethy/nvim-treesitter-textsubjects)

  | Mode | Mapping | Effect                                         |
  | ---- | ------- | ---------------------------------------------- |
  | V    | `.`     | Grow selection to the surrounding text subject |
  | V    | `;`     | Select the outer container                     |
  | V    | `i;`    | Select inside the outer container              |
  | V    | `,`     | Shrink selection to inner text subject         |

- [nvim-treesitter-textobjects](https://github.com/nvim-treesitter/nvim-treesitter-textobjects)

  | Object | Selects             |
  | ------ | ------------------- |
  | `af`   | `a`round `f`unction |
  | `if`   | `i`nside `f`unction |
  | `ac`   | `a`round `c`lass    |
  | `ic`   | `i`nside `c`lass    |
  | `as`   | `a`round `s`cope    |

- [vim-illuminate](https://github.com/RRethy/vim-illuminate)

  | Mode | Mapping | Effect                 |
  | ---- | ------- | ---------------------- |
  | N    | `<A-N>` | Jump to next reference |
  | N    | `<A-P>` | Jump to prev reference |

- [goto-preview](https://github.com/rmagatti/goto-preview)

  | Mode | Mapping | Effect                    |
  | ---- | ------- | ------------------------- |
  | N    | `gpd`   | Preview definition        |
  | N    | `gpt`   | Preview type definition   |
  | N    | `gpi`   | Preview implementation    |
  | N    | `gpr`   | Preview references        |
  | N    | `gP`    | Close all preview windows |

- [trouble.nvim](https://github.com/folke/trouble.nvim)

  | Mode | Mapping    | Effect                  |
  | ---- | ---------- | ----------------------- |
  | C    | `:Trouble` | Open the Trouble window |

  In the trouble window:

  | Mapping           | Effect                                               |
  | ----------------- | ---------------------------------------------------- |
  | `q`               | Close the window                                     |
  | `<CR>` \| `<Tab>` | Jump to the diagnostic in the file                   |
  | `o`               | Jump to the diagnostic in the file and close Trouble |
  | `<C-S>`           | Open diagnotic location in a split                   |
  | `<C-V>`           | Open diagnotic location in a vertical split          |
  | `<C-V>`           | Open diagnotic location in a tab                     |

## Misc

- [neogen](https://github.com/danymat/neogen)

  | Mode | Mapping          | Effect                                   |
  | ---- | ---------------- | ---------------------------------------- |
  | C    | `:Neogen [type]` | Generate a doc comment of the given type |

  Where `[type]` is `func`, `class`, `type`, or `file`. If not give, default is `func`.

- [nerdcommenter](https://github.com/preservim/nerdcommenter)

  | Mode | Mapping            | Effect                                  |
  | ---- | ------------------ | --------------------------------------- |
  | N,V  | `<leader>cc`       | Comment with single line comment        |
  | N,V  | `<leader>cs`       | Comment with multi-line "sexy" comment  |
  | N,V  | `<leader>cm`       | Comment with multi-line minimal comment |
  | N,V  | `<leader>c<Space>` | Toggle comment                          |

# Git Plugins

- [gitlinker.nvim](https://github.com/ruifm/gitlinker.nvim)

  | Mode | Mapping      | Effect                                                   |
  | ---- | ------------ | -------------------------------------------------------- |
  | N,V  | `<leader>gy` | Copy the URL for the current/selected lines to clipboard |
  | N,V  | `<leader>gb` | Open the URL for the current/selected lines in browser   |

- [gitsigns.nvim](https://github.com/lewis6991/gitsigns.nvim)

  | Mode | Mapping      | Effect                            |
  | ---- | ------------ | --------------------------------- |
  | N    | `]c`         | Next hunk                         |
  | N    | `[c`         | Prev hunk                         |
  | N,V  | `<leader>hs` | Stage current hunk/selected lines |
  | N,V  | `<leader>hr` | Reset current hunk/selected lines |
  | N    | `<leader>hu` | Undo staging hunk                 |
  | N    | `<leader>hS` | Stage current file                |
  | N    | `<leader>hR` | Reset current file                |
  | N    | `<leader>hp` | Preview current hunk diff         |
  | N    | `<leader>tb` | Toggle current line blame         |
  | N    | `<leader>hd` | Diff file against the index       |
  | N    | `<leader>hD` | Diff file against the last commit |
  | N    | `<leader>td` | Toggle showing deleted lines      |

- [neogit](https://github.com/TimUntersberger/neogit)

  | Mode | Mapping   | Effect                 |
  | ---- | --------- | ---------------------- |
  | C    | `:Neogit` | Open the Neogit window |

- [diffview.nvim](https://github.com/sindrets/diffview.nvim)

  | Mode | Mapping         | Effect                                      |
  | ---- | --------------- | ------------------------------------------- |
  | C    | `:DiffviewOpen` | Open Diffview                               |
  | N    | `<leader>co`    | Choose the OURS version of the conflict     |
  | N    | `<leader>ct`    | Choose the THEIRS version of the conflict   |
  | N    | `<leader>cb`    | Choose the BASE version of the conflict     |
  | N    | `<leader>ca`    | Choose all versions of the conflict         |
  | N    | `dx`            | Choose none of the versions of the conflict |

- [git-messenger.vim](https://github.com/rhysd/git-messenger.vim)

  | Mode | Mapping      | Effect                          |
  | ---- | ------------ | ------------------------------- |
  | N    | `<leader>gm` | Commit message for current line |

  In the commit message window:

  | Mapping | Effect                           |
  | ------- | -------------------------------- |
  | `o`     | View message for previous commit |
  | `O`     | View message for next commit     |
  | `d`     | View diff for the commit         |

# UI Plugins

- [focus.nvim](https://github.com/beauwilliams/focus.nvim)

  | Mode | Mapping        | Effect               |
  | ---- | -------------- | -------------------- |
  | C    | `:FocusToggle` | Toggle auto resizing |

- [nvim-bqf](https://github.com/kevinhwang91/nvim-bqf)

  In the quickfix window:

  | Mapping | Effect                            |
  | ------- | --------------------------------- |
  | `<C-V>` | Open the item in a vertical split |
  | `<C-S>` | Open the item in a split          |
  | `<C-T>` | Open the item in a tab            |
  | `zf`    | Search entries with fzf           |

- [todo-comments.nvim](https://github.com/folke/todo-comments.nvim)

  | Mode | Mapping          | Effect                               |
  | ---- | ---------------- | ------------------------------------ |
  | C    | `:TodoQuickFix`  | Open Todo entries in quickfix window |
  | C    | `:TodoTelescope` | Open Todo entries in telescope       |
