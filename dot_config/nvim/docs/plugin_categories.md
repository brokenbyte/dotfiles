[toc]

# Utility

- [packer.nvim](https://github.com/wbthomason/packer.nvim)
  - Plugin manager
- [impatient](https://github.com/lewis6991/impatient.nvim)
  - Improve startup time with caching
- [vim-startuptime](https://github.com/dstein64/vim-startuptime)
  - Profile startup time
- [popup.nvim](https://github.com/nvim-lua/popup.nvim)
  - Popup window helper, is a dependency of some other plugins
- [plenary.nvim](https://github.com/nvim-lua/plenary.nvim)
  - Collection of lua functions, is a dependency of some other plugins
- [neorg](https://github.com/nvim-neorg/neorg)
  - Notes/organization plugin
- [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter)
  - Treesitter integration
- [vim-repeat](https://github.com/tpope/vim-repeat)
  - Allows plugin commands to be repeatable with `.`
- [vim-surround](https://github.com/tpope/vim-surround)
  - Easily surround text in brackets/quotes/etc
- [vim-eunuch](https://github.com/tpope/vim-eunuch)
  - Vim plugin for common unix commands
- [mirror.vim](https://github.com/zenbro/mirror.vim)
  - Vim plugin for uploading/downloading files via ssh
- [fzf-lua](https://github.com/ibhagwan/fzf-lua/)
  - Integration with [`fzf`](https://github.com/junegunn/fzf)
- [undotree](https://github.com/mbbill/undotree/)
  - Visualization of vim's undo-tree (see `:h undo-tree`)
- [telescope.nvim](https://github.com/nvim-telescope/telescope.nvim)
  - Another fuzzy finder; similar to fzf-lua but a lot of plugins integrate with it directly.
    Sort of the "native" nvim fuzzy finder.
- [vim-floaterm (might switch to toggleterm?)](https://github.com/voldikss/vim-floaterm)
  - Open a terminal in a floating window; also integrates with external tools like [`ranger`](https://github.com/ranger/ranger)
- [nvim-lspconfig](https://github.com/neovim/nvim-lspconfig/)
  - Plugin for configuring nvim's built in LSP
- [editorconfig.nvim](https://github.com/gpanders/editorconfig.nvim)
  - Support for EditorConfig files
- [nvim-treesitter/playground](https://github.com/nvim-treesitter/playground)
  - View Treesitter nodes for current file/write queries interactively
- [presence.nvim](https://github.com/andweeb/presence.nvim)
  - Discord rich presence integration
- [vim-slash](https://github.com/junegunn/vim-slash)
  - Improved searching; clear highlight on cursor move and improved `*` search
- [vim-unimpaired](https://github.com/tpope/vim-unimpaired)
  - Collection of mappings for various vim features
- [nvim-gomove](https://github.com/booperlv/nvim-gomove)
  - Move/duplicate selected blocks of text
- [numb.nvim](https://github.com/nacro90/numb.nvim)
  - Peek lines with `:N` without jumping there
- [targets.vim](https://github.com/wellle/targets.vim)
  - Collection of additional text objects
- [vim-wordmotion](https://github.com/chaoren/vim-wordmotion)
  - Smarter word motions
- [vim-after-object](https://github.com/junegunn/vim-after-object)
  - Adds an 'after' text object, e.g. `ca=` to change after an `=`

# Development

## Autocomplete

- [nvim-cmp](https://github.com/hrsh7th/nvim-cmp)
  - Autocomplete engine
- [cmp-nvim-lsp](https://github.com/hrsh7th/cmp-nvim-lsp)
  - cmp source for native LSP
- [cmp-cmdline](https://github.com/hrsh7th/cmp-cmdline)
  - cmp source for `:` commands
- [cmp-buffer](https://github.com/hrsh7th/cmp-buffer)
  - cmp source for words in open buffers
- [cmp-path](https://github.com/hrsh7th/cmp-path)
  - cmp source for file paths
- [cmp-nvim-lua](https://github.com/hrsh7th/cmp-nvim-lua)
  - cmp source for nvim lua functions
- [cmp-calc](https://github.com/hrsh7th/cmp-calc)
  - cmp source for doing math in a buffer
- [cmp_luasnip](https://github.com/saadparwaiz1/cmp_luasnip)
  - cmp source for LuaSnip
- [vim-dadbod-completion](https://github.com/kristijanhusak/vim-dadbod-completion)
  - cmp source for vim-dadbod
- [LuaSnip](https://github.com/L3MON4D3/LuaSnip)
  - Snippet engine
- [crates.nvim](https://github.com/Saecki/crates.nvim)
  - Completion for Cargo.toml files
- [nvim-autopairs](https://github.com/windwp/nvim-autopairs)
  - Auto match brackets/quotes
- [nvim-ts-autotag](https://github.com/windwp/nvim-ts-autotag)
  - Auto match html tags

## Language Specific

- [rust-tools.nvim](https://github.com/simrat39/rust-tools.nvim)
  - Better rust experience
- [go.nvim](https://github.com/ray-x/go.nvim)
  - Better go experience
- [vimtex](https://github.com/lervag/vimtex)
  - Better latex experience
- [vim-dadbod](https://github.com/tpope/vim-dadbod)
  - Better SQL experience
- [markdown-preview.nvim](https://github.com/iamcco/markdown-preview.nvim)
  - Markdown live preview

## Debugging

- [nvim-dap](https://github.com/mfussenegger/nvim-dap)
  - [DAP](https://microsoft.github.io/debug-adapter-protocol/) integration
- [nvim-dap-ui](https://github.com/rcarriga/nvim-dap-ui)
  - Nice UI for nvim-dap

## Formatting

- [formatter.nvim](https://github.com/mhartington/formatter.nvim)
  - Integration with various external formatters
- [vim-easy-align](https://github.com/junegunn/vim-easy-align)
  - Quickly align a block of text around a character, e.g. align all `=`

## Navigation

- [aerial.nvim](https://github.com/stevearc/aerial.nvim)
  - Tagbar/document symbol outline
- [nvim-treesitter-textsubjects](https://github.com/RRethy/nvim-treesitter-textsubjects)
  - Fuzzy Treesitter incremental selection
- [nvim-treesitter-textobjects](https://github.com/nvim-treesitter/nvim-treesitter-textobjects)
  - Smart Treesitter text objects
- [vim-illuminate](https://github.com/RRethy/vim-illuminate)
  - Highlight occurences of the symbol under the cursor, uses LSP when available
- [goto-preview](https://github.com/rmagatti/goto-preview)
  - Show goto-definition in a floating window
- [trouble.nvim](https://github.com/folke/trouble.nvim)
  - A pretty list for showing diagnostics, references, telescope results, quickfix and location lists
- [quick-scope](https://github.com/unblevable/quick-scope)
  - Faster `f`/`F`/`t`/`T` movement by highlighting characters

## Misc

- [neogen](https://github.com/danymat/neogen)
  - Doc comment generator
- [nerdcommenter](https://github.com/preservim/nerdcommenter)
  - Comment/uncomment lines
- [nvim-ufo](https://github.com/kevinhwang91/nvim-ufo)
  - Creates folds based on LSP
- [live-server](https://github.com/manzeloth/live-server)
  - Live server for editing HTML/JS
- [null-ls](https://github.com/jose-elias-alvarez/null-ls.nvim)
  - LSP source for non-LSP things
- [vim-dispatch](https://github.com/tpope/vim-dispatch)
  - Async build helper
- [mason.nvim](https://github.com/williamboman/mason.nvim)
  - Nvim package manager for LSP, formatters, DAP, and linters
- [mason-lspconfig.nvim](https://github.com/williamboman/mason-lspconfig.nvim)
  - lspconfig integration for Mason

# Git Plugins

- [vim-fugitive](https://github.com/tpope/vim-fugitive)
  - git wrapper/integration
- [gitlinker.nvim](https://github.com/ruifm/gitlinker.nvim)
  - Copy links to lines of code
- [gitsigns.nvim](https://github.com/lewis6991/gitsigns.nvim)
  - Show git status in signcolumn
- [neogit](https://github.com/TimUntersberger/neogit)
  - git UI (like magit)
- [diffview.nvim](https://github.com/sindrets/diffview.nvim)
  - Enhanced diff viewer
- [git-messenger.vim](https://github.com/rhysd/git-messenger.vim)
  - Commit message for current line

# UI Plugins

- [nvim-treesitter-context](https://github.com/nvim-treesitter/nvim-treesitter-context)
  - Show current context at top of the screen
- [lsp_signature.nvim](https://github.com/ray-x/lsp_signature.nvim)
  - Show signature preview while calling a function
- [telescope-ui-select.nvim](https://github.com/nvim-telescope/telescope-ui-select.nvim)
  - Use telescope for `vim.ui.select`
- [lspkind-nvim](https://github.com/onsails/lspkind.nvim)
  - VSCode-like pictograms
- [nvim-colorizer.lua](https://github.com/NvChad/nvim-colorizer.lua)
  - Highlight colors in the file
- [alpha-nvim](https://github.com/goolord/alpha-nvim)
  - Nvim start screen
- [focus.nvim](https://github.com/beauwilliams/focus.nvim)
  - Auto resize splits
- [calvera-dark.nvim](https://github.com/hoprr/calvera-dark.nvim/)
  - Colorscheme
- [lualine.nvim](https://github.com/nvim-lualine/lualine.nvim)
  - Status/tabline
- [barbecue.nvim](https://github.com/utilyre/barbecue.nvim)
  - Context breadcrumbs in `winbar`
- [vim-kitty](https://github.com/utilyre/barbecue.nvim)
  - [Kitty](https://sw.kovidgoyal.net/kitty/) syntax highlighting
- [vim-mipssyntax](https://github.com/harenome/vim-mipssyntax)
  - MIPS asm syntax
- [nvim-bqf](https://github.com/kevinhwang91/nvim-bqf)
  - Improved quickfix window
- [todo-comments.nvim](https://github.com/folke/todo-comments.nvim)
  - Highlighted TODO comments
- [nvim-scrollview](https://github.com/dstein64/nvim-scrollview)
  - Window scrollbar
- [nvim-web-devicons](https://github.com/nvim-tree/nvim-web-devicons)
  - File icons
- [nvim-ts-rainbow](https://github.com/mrjones2014/nvim-ts-rainbow)
  - Color matching brackets
- [vim-dadbod-ui](https://github.com/kristijanhusak/vim-dadbod-ui)
  - UI for vim-dadbod
- [vim-smoothie](https://github.com/psliwka/vim-smoothie)
  - Smooth scrolling
- [indent-blankline.nvim](https://github.com/lukas-reineke/indent-blankline.nvim)
  - Indent/scope lines
- [nvim-notify](https://github.com/rcarriga/nvim-notify/)
  - Pop ups for `vim.notify`
- [fidget.nvim](https://github.com/j-hui/fidget.nvim)
  - LSP status indicator
