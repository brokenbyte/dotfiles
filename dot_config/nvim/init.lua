-- Enable 24-bit colors
vim.o.termguicolors = true -- Needs to be set before certain plugins load

require("plugins")

require("impatient") -- .enable_profile()
require("config.keybinds")

vim.o.scrolloff = 999

vim.opt.list = true

vim.o.encoding = "UTF-8"

-- Folds smaller than this cannot be closed
vim.o.foldminlines = 10

vim.o.foldcolumn = "3"
vim.o.foldlevel = 99
vim.o.foldlevelstart = 99
vim.o.foldenable = true

vim.o.updatetime = 250

-- Open new splits on the right
vim.o.splitright = true

-- Disable line wrapping
vim.o.wrap = false

-- Enable sign colum (for git status)
vim.o.signcolumn = "yes"

--  Show substitution live
vim.o.inccommand = "split"

-- Disable mouse (annoying with so=999)
vim.o.mouse = ""

-- Underline the cursor's current line
vim.o.cursorline = true

-- Enable auto indent
vim.o.autoindent = true

-- Force dark background
vim.o.background = "dark"

-- Turn on line numbers
vim.o.number = true

-- Number of spaces to use for auto indentation
vim.o.shiftwidth = 4

-- Uses this many spaces in place of hard tabs, i.e. one <TAB> equals this many spaces
vim.o.softtabstop = 4

-- Number of spaces to replace a tab with
vim.o.tabstop = 4

-- Searches for words live as you type, see :help incsearch
vim.o.incsearch = true

-- Replace tabs with spaces
vim.o.expandtab = true

-- Case insensitive searching
vim.o.ignorecase = true

-- Override ignorecase if search contains caps
vim.o.smartcase = true

-- Highlights matching parens, braces, etc. Press % to jump to it.
vim.o.showmatch = true

-- Keep buffers open in background when the window is closed
vim.o.hidden = true

-- Allow directory specific nvim configuration
vim.o.exrc = true

-- Limit the things a `.exrc` file can do
vim.o.secure = true

-- NOTES:
-- :verbose set formatoptions -- Shows the last thing to set a command
