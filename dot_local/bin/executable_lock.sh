#!/bin/sh

echo "Starting locker"

echo "Turning off notifications"
dunstctl set-paused true

muted=$(amixer get Master | grep off)

echo "Muting audio"
amixer set Master mute

echo "Pausing music"
playerctl -a pause

echo "locking screen" && i3lock --indicator -k -f -c000000 --inside-color=99999999 -e -n && echo "unlocking"

if [ "$muted" == "" ]; then
  echo "Unmuting audio"
  amixer set Master unmute
fi

echo "Turning on notifications"
dunstctl set-paused false


