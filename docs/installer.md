# Installer Usage
The installation process and tooling described here is tightly integrated with a
dotfile repo managed by [chezmoi](https://chezmoi.io) and intended to be run on a minimal [archlinux](https://archlinux.org)
system (potentially even from the live ISO), but could be easily ported to other managers and distributions
if you care to edit all the scripts and find a suitable way to initiate the process.

The script is intended to be able to run on as minimal of an arch install as possible, thus minimizing the amount
of work you need to do to configure your system, however it does not _install_ arch for you; it is just intended
to configure an existing system for your user. Here is the minimum intended steps to take for a fully
deployed and configured system:

1. Boot into the arch live iso
1. partition and format your disk(s) as you see fit
1. mount your root partition and any others as needed
1. `pacstrap /mnt base base-devel chezmoi grub efibootmgr` (basics to get the system to boot + configs)
1. If desired, enable `ParallelDownloads` for your new install: `vim /mnt/etc/pacman.conf`
   - note that if you were to install an editor such as `vim` or `neovim` via pacstrap or pacman directly,
     any associated setup scripts ([see below](#how-it-works)) will not be run as they are only run when installed by the scripts
1. `chroot` into the system: `arch-chroot /mnt`
1. Create your newuser: `useradd -m $USERNAME`
1. Configure `sudo` access for them: `echo "username ALL=(ALL:ALL) ALL" >> /etc/sudoers` (or edit before `arch-chroot`)
1. Set a password for your user: `passwd $USERNAME`
1. Initialize the chezmoi repo: `chezmoi init https://gitlab.com/brokenbyte/dotfiles`
1. Deploy dotfiles: `chezmoi apply`
1. Answer a few questions, wait for it to finish, then re-login and you should be done!


# How it works

The installation process is started by a `chezmoi apply`, which will deploy all dotfiles to their desired
location, then once finished will execute the post install hook located at

```.
.chezmoiscripts/install/run_after_install-packages.sh
```

This script does a small amount of setup then utilizes the files under the `install/` directory to
determine what packages to install and what scripts to run after to finish configuring them.
The structure of the `install/` folder is as follows:
```
install
├── packages    // Contains files for packages to install and scripts for configuring them
│   ├── chassis // "Top level" list of packages to install per chassis type 
│   ├── include // Lists of packages that can be included in other include files
│   └── setup   // Scripts for configuring installed packages
│
└── scripts     // Scripts used by the installer
    ├── awk
    └── shell
```
When the script starts, it sources all the shell scripts in `install/scripts/shell` which it uses
for determining what chassis it's installing for (laptop, desktop, server), then it reads, stores, and 
validates the user's `sudo` password to be used for installing packages and running any configuration
commands that need elevated privileges (like enabling `systemd` units).

Then it checks for `cargo` and installs `rustup` and the stable tool chain if it's missing,
and similarly checks for `paru` and installs it from the AUR if missing.

Then it builds a list of packages to install from each source using the sourced helper scripts, populating
the `REPO_PACKAGES`, `AUR_PACKAGES`, and `CARGO_PACKAGES` arrays appropriately. Once these arrays are
populated, the packages are installed if any are present.

If any new packages were installed, then all shell scripts in `install/packages/setup` are sourced.
The names of the files aren't relevent so long as they end in `.sh` since the glob used to find them is 
`**/*.sh`. The only thing that matters is that there exists a function named `setup_[package name]`, e.g. `setup_neovim`
or `setup_zsh`. For each package that is installed, the installer will look for a function named following
that pattern and run it if found.

The installer script provides 2 variables that may be useful in these scripts:
- `$SUDO_FILE` - The path to a shell script that will be run with `sudo`
- `$USER_FILE` - The path to a shell script that will be run the regular user

The scripts can `echo` any commands they need to be run into the respective file and it will be 
executed after all `setup_` scripts have run, e.g.:
```sh
setup_iwd() {
  {
    echo "systemctl enable --now iwd"
  } >> "$SUDO_FILE"
}
```

## Package and Include file format
The files for specifying packages to be installed follow a very simple CSV format
with 3 columns, representing:

- Where the package is to be installed from (Arch repos[r|R], AUR[a|A], or Cargo[c|C])
- The name of the package to be installed
- A human-readable comment about the package

A comment is not required, but each row must have 3 fields (2 commas) to be parsed correctly.
The files must also end in `.pkgs`. Also note that having a package with the same name in multiple 
repos isn't fully supported as there is no way to run two separate setup scripts for each package;
if two scripts existed the last one sourced would win, and would be run if either of the two packages 
were installed.

The "entry point" into the list of packages to install always starts in `install/packageas/chassis/[CHASSIS].pkgs`,
where `[CHASSIS]` is one of `laptop`, `desktop`, or `server`, e.g. `laptop.pkgs`.

The installer also provides a primitive "include" system, whereby one package list may include another
package list to prevent duplicating packages in multiple files manually. To include another file, add a line
to the desired package file of the following format:
```
#{include path/to/include/file}
```
where the path is relative to `install/packages/include`, e.g. if you have the file 
`install/packages/include/gui.pkgs`, your include would be `#{include gui}`. You may also organize your
include files however you wish, including arbitrary subfolders under `include`, and the import works the 
same way. For example if you had `install/packages/include/networking/wifi.pkgs`, you would include it with the line
`#{include networking/wifi}`.

## Helper scripts
This repo also has a couple scripst to assist in maintaining your package lists and are located under
`install/scripts/shell`, and they are called `new_{repo,cargo,aur}_packages.sh`. Running these will 
print out all packages installed from their respective source that are not listen in the package list 
for the current chassis type.
