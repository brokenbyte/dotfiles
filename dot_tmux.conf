# PREFIX+v splits window top to bottom
# PREFIX+s splits window left to right
bind v split-window -h
bind C-v split-window -h
bind s split-window -v
bind C-s split-window -v

# Create new windows without letting go
# of control
bind C-c new-window

# PREFIX+{hjkl} navigates panes
bind h select-pane -L
bind C-h select-pane -L
bind j select-pane -D
bind C-j select-pane -D
bind k select-pane -U
bind C-k select-pane -U
bind l select-pane -R
bind C-l select-pane -R

# Activate without pressing prefix key
# alt+{hjkl} resizes current pane
bind -n M-h resize-pane -L
bind -n M-j resize-pane -D
bind -n M-k resize-pane -U
bind -n M-l resize-pane -R

# Force 256 colors
set-option -sa terminal-overrides ',xterm-256color:RGB'

set -g default-shell $SHELL
set -g default-command $SHELL
set -g default-terminal "screen-256color"

set -g set-clipboard on
set -g set-titles on

# Center align open windows
set -g status-justify "centre"
# Plugins to install
set -g @plugin 'tmux-plugins/tpm'                   # Plugin manager
set -g @plugin 'tmux-plugins/tmux-prefix-highlight' # Prefix-mode indicator
set -g @plugin 'MikeDacre/tmux-zsh-vim-titles'

set -g @prefix_highlight_output_prefix "#[fg=#0097ff]#[bg=#3B4252]#[nobold]#[noitalics]#[nounderscore]#[bg=#0097ff]#[fg=black]"
set -g @prefix_highlight_output_suffix "#[fg=#4C566A]#[nobold]#[noitalics]#[nounderscore]#[bg=brightcyan]#[fg=black]"

# Tmux status line configuration
source-file ~/.tmux/tmuxline.conf

run -b '~/.tmux/plugins/tpm/tpm'
