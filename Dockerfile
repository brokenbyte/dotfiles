FROM archlinux:latest

RUN useradd -m user

RUN pacman -Syu --noconfirm
RUN mkdir /usr/share/doc/ -p
RUN pacman -S  libnotify archey3 zoxide sudo zsh chezmoi vim git kitty neovim exa ranger fd ripgrep pkgfile --noconfirm

RUN echo -ne 'password\npassword\n' | passwd user
RUN echo user 'ALL=(ALL) ALL' >> /etc/sudoers

WORKDIR /home/user

USER user

RUN mkdir -p ~/.config/zsh/gitstatus
RUN git clone --depth=1 https://github.com/romkatv/gitstatus.git ~/.config/zsh/gitstatus

# Set up environment variables
ENV TERM=xterm-256color
ENV DOCKER=1

# Copy just the zshrc template to the container
RUN mkdir -p .local/share/chezmoi/
COPY --chown=user ./dot_zshrc.tmpl .local/share/chezmoi/

# Apply just the zshrc and source it to prevent installing all zsh
# plugins every time we change a file
RUN chezmoi apply --no-tty --no-pager --force ~/.zshrc
RUN /bin/zsh -i -c "zinit update"

# Copy over everything else and install the rest of the dotfiles
COPY --chown=user . .local/share/chezmoi
RUN chezmoi apply --no-tty --no-pager --force

CMD /bin/zsh

