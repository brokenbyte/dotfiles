# brokenbyte's dotfiles

> _Note that these are still a heavy WIP and are subject to change at any time_

This repo utilizes [chezmoi](https://chezmoi.io) to manage my configuration files for tools and applications I use, including:
- neovim
- kitty
- i3
- zsh
- ranger
- rofi
- git
- tmux

It also utilizes [chezmoi's hooks](https://www.chezmoi.io/reference/target-types/#scripts) to handle installing the tools above as well as 
all other tools and their dependencies. Documentation on the installation scripts can be found [here](docs/installer.md).
