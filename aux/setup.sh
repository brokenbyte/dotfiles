#!/bin/bash

pacman -Sy vi chezmoi base-devel --noconfirm
useradd -m user 
echo -en "password\npassword\n" | passwd user 

chown -R user:user /home/user

echo "user ALL=(ALL:ALL) ALL" >> /etc/sudoers

echo "user:password" | chpasswd
