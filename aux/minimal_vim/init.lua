local ensure_packer = function()
    local fn = vim.fn
    local install_path = fn.stdpath("data") .. "/site/pack/packer/opt/packer.nvim"
    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({ "git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", install_path })
        vim.cmd([[packadd packer.nvim]])
        return true
    end
    return false
end

local packer_bootstrap = ensure_packer()

vim.cmd("packadd packer.nvim")
local packer = require("packer")

packer.startup({
    function(use)
        use({
            "wbthomason/packer.nvim",
            opt = true,
        })

        use({
            "yashguptaz/calvera-dark.nvim",
            config = function()
                require("calvera").set()
                vim.cmd("colorscheme calvera")
            end,
        })

        if packer_bootstrap then require("packer").sync() end
    end,
})

vim.o.termguicolors = true

vim.o.encoding = "UTF-8"

vim.o.background = "dark"

vim.o.shiftwidth = 4

vim.o.number = true

vim.o.signcolumn = "yes"
