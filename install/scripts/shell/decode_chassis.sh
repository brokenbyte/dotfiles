#!/bin/bash 

function decode_chassis() {
  local code
  code=$(cat /sys/devices/virtual/dmi/id/chassis_type)
  case $code in
    8|9|10|11|12|14|18|21|31)
      echo "laptop"
      ;;
    3|4|5|6|7|15|16)
      echo "desktop"
      ;;
    23)
      echo "server"
      ;;
    *)
      # Unsupported/unknown chassis code
  esac
}
