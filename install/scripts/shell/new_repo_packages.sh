#!/bin/bash 
#
#shellcheck disable=SC2034,SC2068,SC1091

function new_repo_packages(){
  declare -F decode_chassis > /dev/null || source "$(dirname "$0")/"../shell/decode_chassis.sh
  PROFILE="${PROFILE:-$(decode_chassis)}"
  echo profile is "$PROFILE"


  AWKSCRIPTS=${AWKSCRIPTS:-$(dirname "$0")/../awk}
  PACKAGES=${PACKAGES:-$(dirname "$0")/../../packages}

  excluded_packages=("linux" "base")

  while IFS=, read -r source package _comment; do
    if [[ "$source" = '#'* ]]; then
      continue
    fi

    if [[ $DEBUG ]]; then echo "$source -- $package"; fi

    case "$source" in
      R|r)
        excluded_packages+=("$package")
        ;;
      *)
        continue
        ;;
    esac
  done < <(awk -f "$AWKSCRIPTS"/packages.awk "$PACKAGES"/chassis/"$PROFILE".pkgs)

  installed=$(comm -23 <(pacman -Qqen | sort) <({ pacman -Qqg base-devel; pacman -Si base|sed -n '/Depends\ On/,/:/p'|sed '$d'|cut -d: -f2; } | sort -u))

  NEW_REPO_PACKAGES+=$(echo ${excluded_packages[@]} ${excluded_packages[@]} ${installed[@]}  | tr ' ' '\n' | sort | uniq -u)

}

if [[ $(basename "$0") = "new_repo_packages.sh" ]]; then
  new_repo_packages

  if [[ ${#NEW_REPO_PACKAGES[@]} -ne 0 && "${NEW_REPO_PACKAGES[0]}" != "" ]]; then
    echo "New repo packages:"
    expac -H M "r,%-20n,%d" ${NEW_REPO_PACKAGES[@]}
  fi
fi
