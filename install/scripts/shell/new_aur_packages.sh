#!/bin/bash 
#
#shellcheck disable=SC2034,SC2068,SC1091

function new_aur_packages(){
  declare -F decode_chassis > /dev/null || source "$(dirname "$0")/"../shell/decode_chassis.sh
  PROFILE="$(decode_chassis)"

  AWKSCRIPTS=${AWKSCRIPTS:-$(dirname "$0")/../awk}
  PACKAGES=${PACKAGES:-$(dirname "$0")/../../packages}

  excluded_packages=("paru")

  # TODO: Maybe do this in a more modular way at some point
  while IFS=, read -r source package _comment; do
    if [[ "$source" = '#'* ]]; then
      continue
    fi

    if [[ $DEBUG ]]; then echo "$source -- $package"; fi

    case "$source" in
      A|a)
        excluded_packages+=("$package")
        ;;
      *)
        continue
        ;;
    esac
  done < <(awk -f "$AWKSCRIPTS"/packages.awk "$PACKAGES"/chassis/"$PROFILE".pkgs)

  installed=$(pacman -Qem | cut -d' ' -f 1 )

  NEW_AUR_PACKAGES+=$(echo ${excluded_packages[@]} ${excluded_packages[@]} ${installed[@]}  | tr ' ' '\n' | sort | uniq -u)
}

if [[ $(basename "$0") = "new_aur_packages.sh" ]]; then
  new_aur_packages

  if [[ ${#NEW_AUR_PACKAGES[@]} -ne 0 && "${NEW_AUR_PACKAGES[0]}" != "" ]]; then
    echo "New AUR packages:"
    for p in ${NEW_AUR_PACKAGES[@]}; do
      printf  "\t%s\n" "$p"
    done
  fi
fi
