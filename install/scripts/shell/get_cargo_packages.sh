#!/bin/bash 
#
#shellcheck disable=SC2034,SC2068,SC1091

function get_cargo_packages(){
  echo "in cargo function"
  installed=$(cargo install --list | grep -E "^[[:alpha:]]" | cut -d' ' -f 1)

  declare -F decode_chassis > /dev/null || source "$(dirname "$0")/"../shell/decode_chassis.sh
  PROFILE="$(decode_chassis)"

  AWKSCRIPTS=${AWKSCRIPTS:-$(dirname "$0")/../awk}
  PACKAGES=${PACKAGES:-$(dirname "$0")/../../packages}

  wanted_packages=()

  # TODO: Maybe do this in a more modular way at some point
  while IFS=, read -r source package _comment; do
    if [[ "$source" = '#'* ]]; then
      continue
    fi

    if [[ $DEBUG ]]; then echo "$source -- $package"; fi

    case "$source" in
      C|c)
        wanted_packages+=("$package")
        ;;
      *)
        continue
        ;;
    esac
  done < <(awk -f "$AWKSCRIPTS"/packages.awk "$PACKAGES/chassis/$PROFILE".pkgs)

  CARGO_PACKAGES+=$(echo ${installed[@]} ${installed[@]} ${wanted_packages[@]}  | tr ' ' '\n' | sort | uniq -u)
}

if [[ $(basename "$0") = "get_cargo_packages.sh" ]]; then
  get_cargo_packages

  if [[ ${#CARGO_PACKAGES[@]} -ne 0 && "${CARGO_PACKAGES[0]}" != "" ]]; then
    echo "Cargo packages:"
    for p in ${CARGO_PACKAGES[@]}; do
      printf "\t%s\n" "$p"
    done
  fi
fi
