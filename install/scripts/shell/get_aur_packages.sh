#!/bin/bash 
#
#shellcheck disable=SC2034,SC2068,SC1091

function get_aur_packages(){
  declare -F decode_chassis > /dev/null || source "$(dirname "$0")/"../shell/decode_chassis.sh
  PROFILE="$(decode_chassis)"

  AWKSCRIPTS=${AWKSCRIPTS:-$(dirname "$0")/../awk}
  PACKAGES=${PACKAGES:-$(dirname "$0")/../../packages}

  wanted_packages=()

  while IFS=, read -r source package _comment; do
    if [[ "$source" = '#'* ]]; then
      continue
    fi

    if [[ $DEBUG ]]; then echo "$source -- $package"; fi

    case "$source" in
      A|a)
        wanted_packages+=("$package")
        ;;
      *)
        continue
        ;;
    esac
  done < <(awk -f "$AWKSCRIPTS"/packages.awk "$PACKAGES"/chassis/"$PROFILE".pkgs)

  installed=$(pacman -Qem | cut -d' ' -f 1 )

  AUR_PACKAGES+=$(echo ${installed[@]} ${installed[@]} ${wanted_packages[@]} | tr ' ' '\n' | sort | uniq -u)
}

if [[ $(basename "$0") = "get_aur_packages.sh" ]]; then
  get_aur_packages

  if [[ ${#AUR_PACKAGES[@]} -ne 0 && "${AUR_PACKAGES[0]}" != "" ]]; then
    echo "AUR packages:"
    for p in ${AUR_PACKAGES[@]}; do
      printf "\t%s\n" "$p"
    done
  fi
fi
