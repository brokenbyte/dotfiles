#!/bin/bash 
#
#shellcheck disable=SC2034,SC2068,SC1091

function get_repo_packages(){
  declare -F decode_chassis > /dev/null || source "$(dirname "$0")/"../shell/decode_chassis.sh
  PROFILE="${PROFILE:-$(decode_chassis)}"

  AWKSCRIPTS=${AWKSCRIPTS:-$(dirname "$0")/../awk}
  PACKAGES=${PACKAGES:-$(dirname "$0")/../../packages}
  PACKAGE_FILE="$PACKAGES"/chassis/"$PROFILE".pkgs

  wanted_packages=()

  while IFS=, read -r source package _comment; do
    if [[ "$source" = '#'* ]]; then
      continue
    fi

    if [[ $DEBUG ]]; then echo "$source -- $package"; fi

    case "$source" in
      R|r)
        wanted_packages+=("$package")
        ;;
      *)
        continue
        ;;
    esac
  done < <(awk -f "$AWKSCRIPTS"/packages.awk "$PACKAGE_FILE")

  installed=$(comm -23 <(pacman -Qqen | sort) <({ pacman -Qqg base-devel; pacman -Si base|sed -n '/Depends\ On/,/:/p'|sed '$d'|cut -d: -f2; } | sort -u))

  REPO_PACKAGES+=$(echo ${installed[@]} ${installed[@]} ${wanted_packages[@]} | tr ' ' '\n' | sort | uniq -u)
}

if [[ $(basename "$0") = "get_repo_packages.sh" ]]; then
  get_repo_packages

  if [[ ${#REPO_PACKAGES[@]} -ne 0 && "${REPO_PACKAGES[0]}" != "" ]]; then
    echo "Repo packages:"
    for p in ${REPO_PACKAGES[@]}; do
      printf "\t%s\n" "$p"
    done
  fi
fi
