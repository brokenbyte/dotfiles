#!/bin/bash 
#
#shellcheck disable=SC2034,SC2068,SC1091

function new_cargo_packages(){
  declare -F decode_chassis > /dev/null || source "$(dirname "$0")/"../shell/decode_chassis.sh
  PROFILE="$(decode_chassis)"

  AWKSCRIPTS=${AWKSCRIPTS:-$(dirname "$0")/../awk}
  PACKAGES=${PACKAGES:-$(dirname "$0")/../../packages}

  excluded_packages=()

  while IFS=, read -r source package _comment; do
    if [[ "$source" = '#'* ]]; then
      continue
    fi

    if [[ $DEBUG ]]; then echo "$source -- $package"; fi

    case "$source" in
      C|c)
        excluded_packages+=("$package")
        ;;
      *)
        continue
        ;;
    esac
  done < <(awk -f "$AWKSCRIPTS"/packages.awk "$PACKAGES"/chassis/"$PROFILE".pkgs)

  echo "in cargo function"
  installed=$(cargo install --list | grep -E "^[[:alpha:]]" | cut -d' ' -f 1)

  NEW_CARGO_PACKAGES+=$(echo ${excluded_packages[@]} ${excluded_packages[@]} ${installed[@]}  | tr ' ' '\n' | sort | uniq -u)

}

if [[ $(basename "$0") = "new_cargo_packages.sh" ]]; then
  new_cargo_packages

  if [[ ${#NEW_CARGO_PACKAGES[@]} -ne 0 && "${NEW_CARGO_PACKAGES[0]}" != "" ]]; then
    echo "New cargo packages:"
    for p in ${NEW_CARGO_PACKAGES[@]}; do
      printf  "\t%s\n" "$p"
    done
  fi
fi
