# This script should be given the absolute path to a chassis.pkgs file 


BEGIN {
  delete input[0] # List of packages to install
  delete seen[0]  # List of files we've included already
  len=0
  #INCLUDE_DIR = ENVIRON["INCLUDE_DIR"] -- should be set by the installer script and exported
  INCLUDE_DIR = ENVIRON["HOME"] "/.local/share/chezmoi/install/packages/include/"
  DEBUG = 0
}

function read(path) {
  while ((getline < path) > 0) {
    if (match($0, /#{include (.*)}/, a)) {
      if(contains(seen, a[1])) { # Don't include a file more than once
        if(DEBUG){
          print "Skipping " a[1] "..."
        }
        continue
      }

      seen[a[1]] = 1

      if(DEBUG){
        print "seen " a[1]

        print "Including " a[1] "...\n\n"
      }

      read(INCLUDE_DIR a[1] ".pkgs")

    } else {
      input[len++]=$0
    }
  }
  close(path)
}

function contains(arr, entry) {
  for(e in arr) {
    if(e == entry){
      return 1
    }
  }
  return 0
}

BEGIN {
 read(ARGV[1])
 ARGV[1]=""
 for (i in input)
   print input[i]
}
