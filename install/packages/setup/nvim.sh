#!/bin/bash 

function setup_neovim() {
  echo "In nvim setup"
  {
    if [[ ! -d "$HOME/.local/share/nvim/site/pack/packer/start/packer.nvim" ]]; then
      echo "git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim"
    fi

    echo 'echo Installing nvim plugins...'
    echo 'nvim --headless -c "autocmd User PackerComplete quitall" -c "PackerSync" > /dev/null'
    echo 'echo Done installing nvim plugins'
  } >> "$USER_FILE"
}
