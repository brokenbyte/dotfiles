#!/bin/bash 

function setup_kitty() {
  search="$HOME/.config/kitty/kitty_search"
  grab="$HOME/.config/kitty/kitty_grab"
  if [[ ! -d "$search" ]]; then
    git clone https://github.com/trygveaa/kitty-kitten-search "$search"
  fi

  if [[ ! -d "$grab" ]]; then
    git clone https://github.com/yurikhan/kitty_grab "$grab"
  fi
}
