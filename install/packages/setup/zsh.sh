#!/bin/bash 

function setup_zsh() {
  {
    echo "chsh -s /bin/zsh \$SUDO_USER"
  } >> "$SUDO_FILE"

  if [[ ! -d "$HOME/.config/zsh/gitstatus" ]]; then
    git clone --depth=1 https://github.com/romkatv/gitstatus.git ~/.config/zsh/gitstatus
  fi

  /bin/zsh -i -c "zinit update"
}
